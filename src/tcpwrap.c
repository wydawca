/* wydawca - automatic release submission daemon
   Copyright (C) 2007-2013, 2017, 2019-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include "wydawca.h"

#ifdef WITH_LIBWRAP
# include <tcpd.h>

static int tcpwrap_enable;
static char *tcpwrap_daemon;
int deny_severity = LOG_INFO;
int allow_severity = LOG_INFO;

static int
cb_syslog_priority(enum grecs_callback_command cmd, grecs_node_t * node,
		   void *varptr, void *cb_data)
{
    grecs_locus_t *locus = &node->locus;
    grecs_value_t *value = node->v.value;
    int pri;

    if (wy_assert_string_arg(locus, cmd, value))
	return 1;
    pri = wy_strtopri(value->v.string);
    if (pri == -1)
	grecs_error(locus, 0, _("Unknown syslog priority `%s'"),
		    value->v.string);
    else
	*(int *) varptr = pri;
    return 0;
}

struct grecs_keyword tcpwrapper_kw[] = {
    { "enable", NULL,
      N_("Enable TCP wrapper access control.  Default is \"yes\"."),
      grecs_type_bool, GRECS_DFLT, &tcpwrap_enable },
    { "daemon", N_("name"),
      N_("Set daemon name for TCP wrapper lookups.  "
	 "Default is program name."),
      grecs_type_string, GRECS_DFLT, &tcpwrap_daemon },
    { "allow-table", N_("file"),
      N_("Use file for positive client address access control "
	 "(default: /etc/hosts.allow)."),
      grecs_type_string, GRECS_DFLT, &hosts_allow_table },
    { "deny-table", N_("file"),
      N_("Use file for negative client address access control "
	 "(default: /etc/hosts.deny)."),
      grecs_type_string, GRECS_DFLT, &hosts_deny_table },
    { "allow-syslog-priority", N_("prio"),
      N_("Log host allows at this syslog priority."),
      grecs_type_string, GRECS_DFLT, &allow_severity, 0,
      cb_syslog_priority },
    { "deny-syslog-priority", N_("prio"),
      N_("Log host denies at this syslog priority."),
      grecs_type_string, GRECS_DFLT, &deny_severity, 0,
      cb_syslog_priority },
    {NULL}
};

int
tcpwrap_access(int fd)
{
    struct request_info req;

    if (!tcpwrap_enable)
	return 1;
    request_init(&req,
		 RQ_DAEMON,
		 tcpwrap_daemon ? tcpwrap_daemon : program_name,
		 RQ_FILE, fd, NULL);
    fromhost(&req);
    return hosts_access(&req);
}

#endif
