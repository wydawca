/* wydawca - automatic release submission daemon
   Copyright (C) 2007, 2009-2013, 2017, 2019-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include "wydawca.h"
#include <sys/inotify.h>
#include <sys/ioctl.h>

/* A directory watcher is described by the following structure */
struct dirwatcher {
    LIST_ENTRY(dirwatcher) link;
    struct spool *spool;
    int wd;			/* Watch descriptor */
};

static LIST_HEAD(,dirwatcher) dirwatcher_list;

/* Find a watcher with the given descriptor */
static struct dirwatcher *
dirwatcher_find_wd(int wd)
{
    struct dirwatcher *dwp;

    LIST_FOREACH(dwp, &dirwatcher_list, link) {
	if (dwp->wd == wd)
	    break;
    }
    return dwp;
}

static int
create_watcher(struct spool *sp, void *data)
{
    int ifd = *(int *) data;
    struct dirwatcher *dwp;
    int wd;
    const char *path = sp->source_dir;

    if (!sp)
	return 0;

    if (!sp->inotify_enable) {
	wy_debug(2, ("disabling inotify support for spool %s", sp->tag));
	return 0;
    }

    wy_debug(2, ("spool %s: creating watcher %s", sp->tag, path));
    dwp = malloc(sizeof(*dwp));
    if (!dwp) {
	wy_log(LOG_ERR, "not enough memory");
	return 1;
    }
    dwp->spool = sp;

    wd = inotify_add_watch(ifd, path,
			   IN_DELETE | IN_CREATE | IN_CLOSE_WRITE |
			   IN_MOVED_FROM | IN_MOVED_TO);
    if (wd == -1) {
	wy_log(LOG_ERR, "cannot set watch on %s: %s", path,
	       strerror(errno));
	free(dwp);
	return 1;
    }

    dwp->wd = wd;
    LIST_INSERT_HEAD(&dirwatcher_list, dwp, link);
    return 0;
}

int
watcher_init(void)
{
    int ifd, rc;

    if (!inotify_enable) {
	wy_debug(2, ("disabling inotify support"));
	return -1;
    }

    wy_debug(2, ("setting up inotify"));
    ifd = inotify_init();
    if (ifd == -1) {
	wy_log(LOG_ERR, "inotify_init: %s", strerror(errno));
	return -1;
    }

    rc = for_each_spool(create_watcher, &ifd);
    if (rc)
	exit(EX_OSERR);
    if (LIST_EMPTY(&dirwatcher_list)) {
	wy_debug(2, ("inotify: nothing to watch"));
	close(ifd);
	ifd = -1;
    } else
	wy_debug(2, ("inotify initialized successfully"));

    return ifd;
}

static void
process_event(struct inotify_event *ep)
{
    static struct dirwatcher *dwp;
    dwp = dirwatcher_find_wd(ep->wd);

    if (ep->mask & IN_IGNORED)
	/* nothing */ ;
    else if (ep->mask & IN_Q_OVERFLOW)
	wy_log(LOG_NOTICE, "event queue overflow");
    else if (ep->mask & IN_UNMOUNT)
	/* FIXME: not sure if there's anything to do. Perhaps we should
	   deregister the watched dirs that were located under the mountpoint
	*/ ;
    else if (!dwp) {
	if (ep->name)
	    wy_log(LOG_NOTICE, "unrecognized event %x for %s",
		   ep->mask, ep->name);
	else
	    wy_log(LOG_NOTICE, "unrecognized event %x", ep->mask);
    } else if (ep->mask & IN_CREATE) {
	wy_debug(1, ("%s/%s created", dwp->spool->source_dir, ep->name));
    } else if (ep->mask & (IN_DELETE | IN_MOVED_FROM)) {
	wy_debug(1, ("%s/%s %s", dwp->spool->source_dir,
		     ep->name,
		     ep->mask & IN_DELETE ? "deleted" : "moved out"));
	triplet_remove_file(dwp->spool, ep->name);
    } else if (ep->mask & (IN_CLOSE_WRITE | IN_MOVED_TO)) {
	wy_debug(1, ("%s/%s written", dwp->spool->source_dir, ep->name));
	triplet_enqueue(spool_add_new_file(dwp->spool, ep->name));
    } else
	wy_log(LOG_NOTICE, "%s/%s: unexpected event %x",
	       dwp->spool->source_dir, ep->name, ep->mask);

}

static char buffer[4096];
static int offset;

int
watcher_run(int ifd)
{
    int n;
    int rdbytes;

    if (ioctl(ifd, FIONREAD, &n)) {
	wy_log(LOG_ERR, "ioctl: %s", strerror(errno));
	return -1;
    }
    if (offset + n > sizeof buffer)
	n = sizeof buffer - offset;
    if (n) {
	rdbytes = read(ifd, buffer + offset, n);
	if (rdbytes == -1) {
	    if (errno == EINTR) {
		/* FIXME wy_log (LOG_NOTICE, "got signal %d", signo); */
		return 0;
	    }

	    wy_log(LOG_NOTICE, "read failed: %s", strerror(errno));
	    return -1;
	}
    }
    offset += n;

    for (n = 0; offset - n >= sizeof(struct inotify_event);) {
	struct inotify_event *ep;
	size_t size;

	ep = (struct inotify_event *) (buffer + n);
	size = sizeof(*ep) + ep->len;
	if (offset - n < size)
	    break;

	process_event(ep);

	n += size;
    }
    if (n > 0 && offset - n > 0)
	memmove(buffer, buffer + n, offset - n);
    offset -= n;

    return 0;
}

FILE *
fopenat_ro(int dirfd, char const *name)
{
    int fd;
    FILE *fp;

    fd = openat(dirfd, name, O_RDONLY);
    if (fd == -1)
	return NULL;
    fp = fdopen(fd, "r");
    if (fp == NULL) {
	int ec = errno;
	close(fd);
	errno = ec;
    }
    return fp;
}
