/* wydawca - automatic release submission daemon
   Copyright (C) 2007-2008, 2010-2013, 2017, 2019-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include "wydawca.h"

/* Directive file support */

#define ISSPACE(c) ((c)==' '||(c)=='\t')

/*
 * An auxiliary function to compare two directive lines.
 * If A and B have the same directive name portion, analyze the directive
 * argument (or arguments, in case of "symlink".  If each argument of B
 * is the corresponding argument of A plus the ".sig" suffix, return 1.
 * If the relation is the reverse (arg(A) = arg(B) + ".sig"), return -1.
 * Otherwise, return 0.
 */
static int
str_dirname_sig(char const *a, char const *b)
{
    char const *p = a;
    int arg2;

    while (1) {
	if (*a != *b)
	    return 0;
	if (*a == 0 || *a == ':')
	    break;
	a++;
	b++;
    }

    arg2 = (a-p+1 == sizeof("symlink") && memcmp(p, "symlink", a-p) == 0);

    do {
	if (!*++a)
	    return 0;
    } while (ISSPACE(*a));

    do {
	if (!*++b)
	    return 0;
    } while (ISSPACE(*b));

    while (*a && *b) {
	if (*a != *b) {
	    if (arg2) {
		if (ISSPACE(*a)
		    && strncmp(b, SUF_SIG, SUF_SIG_LEN) == 0) {
		    arg2 = 1;
		    b += 4;
		} else if (ISSPACE(*b)
			   && strncmp(a, SUF_SIG, SUF_SIG_LEN) == 0) {
		    arg2 = -1;
		    a += 4;
		} else
		    return 0;
		while (*a && ISSPACE(*a))
		    a++;
		while (*b && ISSPACE(*b))
		    b++;
	    } else
		return 0;
	}
	a++;
	b++;
    }

    if (*a == 0 && strcmp(b, SUF_SIG) == 0)
	return (arg2 == 0 || arg2 == 1) ? 1 : 0;

    if (*b == 0 && strcmp(a, SUF_SIG) == 0)
	return (arg2 == 0 || arg2 == -1) ? -1 : 0;

    return 0;
}

/* Parse directives from TRP->blurb. Fill TRP->directive */
int
directive_parse(struct wy_triplet *trp)
{
    size_t dcount, i, j;
    char *p;

    wy_debug(3, (_("%s: parsing directive blurb: %s"),
		 trp->file[file_directive].name, trp->blurb));

    dcount = 0;
    for (p = trp->blurb; *p; p++)
	if (*p == '\n')
	    dcount++;

    trp->directive = grecs_calloc(dcount + 1, sizeof trp->directive[0]);
    p = trp->blurb;
    for (i = j = 0; i < dcount; i++) {
	trp->directive[j] = p;
	p = strchr(p, '\n');
	if (p)
	    *p++ = 0;
	if (trim(trp->directive[j]) == 0)	/* ignore empty lines */
	    continue;
	if (strchr(trp->directive[j], ':') == NULL) {
	    wy_log(LOG_ERR, _("%s: invalid line: %s"),
		   trp->file[file_directive].name, trp->directive[j]);
	    free(trp->directive);
	    trp->directive = NULL;
	    return 1;
	}
	if (j > 1) {
	    /*
	     * The gnupload script (as of version 2018-05-19.18) when given the
	     * --symlink-regex option generates two symlink directives in
	     * sequence: the first one for the filename, and second one
	     * for filename.sig.  This contradicts The Automated FTP Uploads
	     * specification, which says that:
	     *
	     *   "The .sig file should not be explicitly mentioned in a
	     *    directive. When you specify a directive to operate on
	     *    a file, its corresponding .sig file will be handled
	     *    automatically."
	     *
	     * The following optimizes this case.
	     */
	    switch (str_dirname_sig(trp->directive[j-1], trp->directive[j])) {
	    case 1:
		continue;
	    case -1:
		trp->directive[j-1] = trp->directive[j];
		continue;
	    }
	}
	j++;
	if (!p)
	    break;
    }
    trp->directive[j] = NULL;
    return 0;
}

/* If a directive KEY exists in the triplet TRP, return 0 and point PVAL
   (unless it is NULL) to its value. Othervise, return 1. */
int
directive_get_value(struct wy_triplet const *trp, const char *key,
		    const char **pval)
{
    int keylen = strlen(key);
    int i;

    for (i = 0; trp->directive[i]; i++) {
	char *str = trp->directive[i];
	int len = strlen(str);
	if (len > keylen && memcmp(str, key, keylen) == 0
	    && str[keylen] == ':') {
	    str += keylen + 1;
	    while (*str && isspace(*str))
		str++;
	    if (pval)
		*pval = str;
	    return 0;
	}
    }
    return 1;
}

/* Auxiliary function for sequential access to directories from TRP.
   Arguments:
     N          - Index of the current directive,
     TRP        - Triplet,
     PKEY, PVAL - Return addresses.

     The function points PKEY and PVAL to the keyword and value of the Nth
     directive, and returns N + 1.

     If N points past all the directive, the function returns 0. */
static int
_directive_seq_get(int n, struct wy_triplet *trp,
		   const char **pkey, const char **pval)
{
    char *p;
    size_t len;

    if (trp->directive[n] == NULL)
	return 0;

    p = strchr(trp->directive[n], ':');
    len = p - trp->directive[n];
    if (len + 1 > trp->tmpsize) {
	trp->tmpsize = len + 1;
	trp->tmp = grecs_realloc(trp->tmp, trp->tmpsize);
    }
    memcpy(trp->tmp, trp->directive[n], len);
    trp->tmp[len] = 0;
    *pkey = trp->tmp;
    for (p++; *p && isspace(*p); p++);
    if (pval)
	*pval = p;
    return ++n;
}

/* Get the first directive from TRP. Point *PKEY to its keyword and
   *PVAL to its value. Return 1 on success, 0 on failure. */
int
directive_first(struct wy_triplet *trp, const char **pkey,
		const char **pval)
{
    int n = 0;
    return _directive_seq_get(n, trp, pkey, pval);
}

/* Get the first directive from TRP. Point *PKEY to its keyword and
   *PVAL to its value. Return 1 on success, 0 on failure.
   Return non-0 on success, 0 on failure */
int
directive_next(struct wy_triplet *trp, int n,
	       const char **pkey, const char **pval)
{
    return _directive_seq_get(n, trp, pkey, pval);
}

/* Pack a directive string VAL into an unsigned number */
int
directive_pack_version(const char *val, unsigned *pversion)
{
    char *p;
    unsigned v;

    v = strtoul(val, &p, 10);
    if (*p != '.')
	return 1;
    p++;
    v *= 100;
    v += strtoul(p, &p, 10);
    if (*p && *p != '.')
	return 1;
    *pversion = v;
    return 0;
}

int
directive_unpack_version(unsigned version, char **pbuf, size_t *psize)
{
    return grecs_asprintf(pbuf, psize, "%u.%u",
			  version / 100, version % 100);
}

/* Return true if the directory file version of the triplet TRP
   is within the inclusive range FROM and TO (packed) */
int
directive_version_in_range_p(struct wy_triplet *trp,
			     unsigned from, unsigned to)
{
    const char *val;
    unsigned version;

    if (directive_get_value(trp, "version", &val)) {
	wy_log(LOG_ERR, _("%s: missing `version' directive"),
	       trp->file[file_directive].name);
	return 0;
    }

    if (directive_pack_version(val, &version)) {
	wy_log(LOG_ERR, _("%s: unparsable version: %s"),
	       trp->file[file_directive].name, val);
	return 0;
    }
    wy_log(LOG_NOTICE, _("%s: VERSION: %s"),
	   trp->file[file_directive].name, val);

    trp->version = version;

    if (from <= version && version <= to)
	return 1;

    wy_log(LOG_ERR, _("%s: version %s is not in the allowed range"),
	   trp->file[file_directive].name, val);
    return 0;
}

enum directive {
    unknown_dir,
    comment_dir,
    directory_dir,
    version_dir,
    filename_dir,
    rmsymlink_dir,
    archive_dir,
    symlink_dir,
    replace_dir,
    MAX_DIRECTIVE
};

struct directive_table {
    const char *name;
    int mandatory;
    int min_version;
};

static struct directive_table directive_table[] = {
    [unknown_dir]   = { "" },
    [comment_dir]   = { "comment",   0 },
    [directory_dir] = { "directory", 1 },
    [version_dir]   = { "version",   1 },
    [filename_dir]  = { "filename",  0 },
    [rmsymlink_dir] = { "rmsymlink", 0 },
    [archive_dir]   = { "archive",   0 },
    [symlink_dir]   = { "symlink",   0 },
    [replace_dir]   = { "replace",   0, 102 },
};

static enum directive
find_directive(const char *key)
{
    int i;

    for (i = 0; i < MAX_DIRECTIVE; i++)
	if (strcmp(directive_table[i].name, key) == 0)
	    return i;
    return unknown_dir;
}

/* Return 0 if the directory file format of the triplet TRP is OK. */
int
verify_directive_format(struct wy_triplet *trp)
{
    int i;
    char const *key, *val;

    /*
     * Verify if file version is OK. This also sets trp->version.
     */
    if (!directive_version_in_range_p(trp, min_directive_version,
				      max_directive_version))
	return 1;

    /* Verify if mandatory directives are present */
    for (i = 0; i < MAX_DIRECTIVE; i++) {
	if (directive_table[i].mandatory &&
	    directive_get_value(trp, directive_table[i].name, NULL)) {
		wy_log(LOG_ERR, _("%s: mandatory directive `%s' is missing"),
		       trp->file[file_directive].name, directive_table[i].name);
		return 1;
	}
    }

    /* Check if there are any unknown and out-of-version directives. */
    for (i = directive_first(trp, &key, NULL); i != 0;
	 i = directive_next(trp, i, &key, NULL)) {
	enum directive dir = find_directive(key);

	if (dir == unknown_dir) {
	    wy_log(LOG_ERR, _("%s:%d: unknown directive `%s'"),
		   trp->file[file_directive].name, i, key);
	    return 1;
	}

	if (trp->version < directive_table[dir].min_version) {
	    wy_log(LOG_ERR, _("%s:%d: directive `%s' is not valid in version %d"),
		   trp->file[file_directive].name, i, key, trp->version);
	    return 1;
	}
    }

    /*
     * Check of the value of filename directive matches the dist file
     * name.
     * FIXME: Perhaps this is overly strict.
     */
    if (trp->file[file_dist].name && trp->file[file_signature].name) {
	if (directive_get_value(trp, "filename", &val)) {
	    wy_log(LOG_ERR, _("%s: missing `filename' directive"),
		   trp->file[file_directive].name);
	    return 1;
	}
	if (strcmp(val, trp->file[file_dist].name)) {
	    wy_log(LOG_ERR,
		   _("%s: filename %s does not match actual name"),
		   trp->file[file_dist].name, val);
	    return 1;
	}
    }

    /*
     * Process early directives:
     */

    /* Configure replace. */
    if (trp->version < 102) {
	trp->replace_allowed = 1;
    } else if (directive_get_value(trp, "replace", &val) == 0) {
	if (strcmp(val, "true") == 0)
	    trp->replace_allowed = 1;
	else
	    trp->replace_allowed = 0;
    } else
	trp->replace_allowed = 0;

    return 0;
}

static char *
save_script(const char *wd, const char *script)
{
    char *file_name;
    mode_t old_mask;
    int fd;
    size_t length;

    file_name = concat_file(wd, "chkXXXXXX", NULL);
    old_mask = umask(0077);
    fd = mkstemp(file_name);
    umask(old_mask);
    if (fd == -1) {
	wy_log(LOG_CRIT,
	       _("cannot create temporary script file %s: %s"),
	       file_name, strerror(errno));
	free(file_name);
	return NULL;
    }

    length = strlen(script);
    while (length) {
	ssize_t wrb = write(fd, script, length);
	if (wrb == -1) {
	    wy_log(LOG_CRIT,
		   _("error writing to temporary script "
		     "file %s: %s"), file_name, strerror(errno));
	    break;
	}
	if (wrb == 0) {
	    wy_log(LOG_CRIT,
		   _("short write to temporary script file %s"),
		   file_name);
	    break;
	}

	length -= wrb;
	script += wrb;
    }
    close(fd);
    if (length) {
	free(file_name);
	return NULL;
    }
    return file_name;
}

static int
stderr_redirector(const char *tag)
{
    int p[2];
    pid_t pid;

    if (pipe(p)) {
	wy_log(LOG_CRIT, "redirector pipe: %s", strerror(errno));
	return -1;
    }

    pid = fork();
    if (pid == -1) {
	wy_log(LOG_CRIT, "redirector fork: %s", strerror(errno));
	return -1;
    }

    if (pid == 0) {
	FILE *fp;
	size_t size = 0;
	char *buf = NULL;

	close(p[1]);
	fp = fdopen(p[0], "r");
	if (!fp)
	    _exit(127);
	while (grecs_getline(&buf, &size, fp) >= 0) {
	    trim_crlf(buf);
	    wy_log(LOG_NOTICE, "%s: %s", tag, buf);
	}
	_exit(0);
    }

    close(p[0]);
    return p[1];
}

static int
run_check_script(const char *script, struct wy_triplet *trp,
		 const char *descr)
{
    char *script_file;
    pid_t pid;
    int status;
    int p[2];
    void (*oldsig) ();
    FILE *fp;
    char *buf;
    size_t size, total;
    char *wd;

    wd = wy_tempdir();
    wy_debug(2, (_("prep script in %s: %20.20s%s"),
		 wd, script, strlen(script) > 20 ? "..." : ""));

    script_file = save_script(wd, script);
    if (!script_file) {
	wy_rmdir_r(wd);
	free(wd);
	return 1;
    }

    wy_debug(2, (_("script file: %s"), script_file));

    if (pipe(p)) {
	wy_log(LOG_CRIT, "pipe: %s", strerror(errno));
	free(script_file);
	wy_rmdir_r(wd);
	free(wd);
	return 1;
    }

    oldsig = signal(SIGCHLD, SIG_DFL);
    pid = fork();
    if (pid == -1) {
	wy_log(LOG_CRIT, "fork: %s", strerror(errno));
	close(p[0]);
	close(p[1]);
	free(script_file);
	wy_rmdir_r(wd);
	free(wd);
	signal(SIGCHLD, oldsig);
	return 1;
    }
    if (pid == 0) {
	int i;
	int efd;
	char *argv[4];
	const struct spool *spool = trp->spool;

	signal(SIGHUP, SIG_DFL);
	signal(SIGTERM, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	signal(SIGINT, SIG_DFL);
	signal(SIGCHLD, SIG_DFL);
	signal(SIGALRM, SIG_DFL);

	efd = stderr_redirector(script_file);
	if (efd == -1)
	    _exit(127);

	/* Select the biggest fd. */
	i = p[1];
	if (efd > i)
	    i = efd;
	/* Close all fds above it. */
	wy_close_fds_above(i);

	if (p[1] != 1 && dup2(p[1], 1) != 1) {
	    wy_log(LOG_CRIT,
		   "cannot duplicate script's stdout: %s",
		   strerror(errno));
	    _exit(127);
	}

	if (efd != 2 && dup2(efd, 2) != 2) {
	    wy_log(LOG_CRIT,
		   "cannot duplicate script's stderr: %s",
		   strerror(errno));
	    _exit(127);
	}

	setenv("WYDAWCA_SPOOL", spool->tag, 1);
	setenv("WYDAWCA_SOURCE", spool->source_dir, 1);
	setenv("WYDAWCA_DEST", wy_url_path(spool->dest_url), 1);
	setenv("WYDAWCA_URL", spool->url, 1);
	setenv("WYDAWCA_TRIPLET_BASE", trp->name, 1);
	setenv("WYDAWCA_DIST_FILE", trp->file[file_dist].name, 1);

	if (chdir(wd)) {
	    wy_log(LOG_CRIT, "cannot change to %s: %s",
		   wd, strerror(errno));
	    _exit(127);
	}

	argv[0] = "sh";
	argv[1] = script_file;
	argv[2] = NULL;

	execv("/bin/sh", argv);
	_exit(127);
    }

    /* Master */
    free(script_file);
    close(p[1]);
    fp = fdopen(p[0], "r");
    buf = NULL;
    size = total = 0;
    wy_debug(3, (_("reading script output...")));
    while (grecs_getline(&buf, &size, fp) > 0) {
	size_t len = strlen(buf);
	wy_debug(3, (_("read: %s"), buf));
	grecs_txtacc_grow(trp->acc, buf, len);
	total += size;
    }
    grecs_txtacc_grow_char(trp->acc, 0);
    wy_debug(3, (_("bytes read: %lu"), (unsigned long) total));

    fclose(fp);

    waitpid(pid, &status, 0);
    signal(SIGCHLD, oldsig);
    wy_rmdir_r(wd);
    free(wd);

    if (total)
	trp->check_diag = grecs_txtacc_finish(trp->acc, 0);
    else
	grecs_txtacc_clear(trp->acc);

    trp->check_result = status;
    if (WIFEXITED(status)) {
	status = WEXITSTATUS(status);
	if (status) {
	    wy_log(LOG_ERR, "%s for %s@%s returned %d",
		   descr, trp->name, trp->spool->tag, status);
	    return 1;
	} else
	    wy_debug(3, ("%s for %s@%s returned %d",
			 descr, trp->name, trp->spool->tag, status));
    } else if (WIFSIGNALED(status)) {
	int sig = WTERMSIG(status);
	wy_log(LOG_NOTICE,
	       "%s for %s@%s terminated on signal %d",
	       descr, trp->name, trp->spool->tag, sig);
	return 1;
    } else {
	wy_log(LOG_NOTICE,
	       "%s for %s@%s terminated with unhandled status",
	       descr, trp->name, trp->spool->tag);
	return 1;
    }
    return 0;
}

static int
external_check(struct wy_triplet *trp)
{
    int rc;
    const struct spool *spool = trp->spool;
    char *file;

    if (!trp->file[file_dist].name)
	return 0;
    if (!spool->check_script && !default_check_script)
	return 0;

    file = concat_file(temp_homedir, trp->file[file_dist].name, NULL);
    if (copy_file(trp->spool->source_fd, trp->file[file_dist].name,
		  AT_FDCWD, file)) {
	free(file);
	return 1;
    }

    rc = 0;
    if (spool->check_script)
	rc |= run_check_script(spool->check_script, trp,
			       _("spool check script"));

    if (rc == 0 && default_check_script)
	rc |= run_check_script(default_check_script, trp,
			       _("default check script"));

    free(file);

    if (rc) {
	wydawca_stat_incr(WY_STAT_CHECK_FAIL);
	notify(&spool->notification_queue, trp, wy_ev_check_fail);
    }

    return rc;
}

static int
symlink_filelist(struct wy_triplet *trp, const char *key, const char *val)
{
    int rc = 0;
    struct wordsplit ws;

    if (wordsplit(val, &ws, WRDSF_DEFFLAGS)) {
	wy_log(LOG_ERR, _("cannot parse symlink value `%s'"), val);
	return 1;
    }

    if (ws.ws_wordc != 2) {
	rc = 1;
	wy_log(LOG_ERR,
	       _("wrong number of arguments to %s directive: `%s'"),
	       key, val);
    } else
	rc = symlink_file(trp, ws.ws_wordv[0], ws.ws_wordv[1]);

    wordsplit_free(&ws);
    return rc;
}

/* Process the directives from TRP */
int
process_directives(struct wy_triplet *trp)
{
    int rc, n;
    const char *key, *val;
    struct spool *spool = trp->spool;

    wydawca_stat_incr(WY_STAT_COMPLETE_TRIPLETS);

    for (n = directive_first(trp, &key, &val); n;
	 n = directive_next(trp, n, &key, &val)) {
	enum directive d = find_directive(key);
	switch (d) {
	case unknown_dir:
	case MAX_DIRECTIVE:
	    /* should not happen */
	    abort();

	case comment_dir:
	    wy_log(LOG_NOTICE, _("%s: COMMENT: %s"),
		   trp->file[file_directive].name, val);
	    break;

	case filename_dir:
	    rc = verify_detached_signature(trp);
	    if (rc == 0) {
		if (external_check(trp))
		    return 1;
		if (move_file(trp, file_dist) ||
		    move_file(trp, file_signature))
		    return 1;
	    } else {
		wy_log(LOG_ERR,
		       _("invalid detached signature for %s"), trp->name);
		return 1;
	    }
	    break;

	case archive_dir:
	    if (archive_file(trp, val))
		return 1;
	    break;

	case symlink_dir:
	    if (symlink_filelist(trp, key, val))
		return 1;
	    break;

	case rmsymlink_dir:
	    if (rmsymlink_file(trp, val))
		return 1;
	    break;

	    /* These have already been processed */
	case replace_dir:
	    /* See verify_directive_format */
	case directory_dir:
	    /* see fill_project_name */
	case version_dir:
	    /* See verify_directive_format and directive_version_in_range_p */
	    break;
	}
    }

    if (!wy_dry_run
	&& unlinkat(trp->spool->source_fd, trp->file[file_directive].name, 0)) {
	wy_log(LOG_CRIT, _("%s: cannot unlink directive file: %s"),
	       trp->file[file_directive].name, strerror(errno));
    }

    wydawca_stat_incr(WY_STAT_TRIPLET_SUCCESS);
    triplet_report_finish(trp);
    notify(&spool->notification_queue, trp, wy_ev_success);
    return 0;
}
