/* wydawca - automatic release submission daemon
   Copyright (C) 2007-2013, 2017, 2019-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include "wydawca.h"
#include <time.h>

/* Triplets are stored in a symtab:  */
static struct grecs_symtab *triplet_table;
static pthread_mutex_t triplet_table_mutex = PTHREAD_MUTEX_INITIALIZER;

/* ... and are organized into a doubly-linked list, using the prev and
   next members of struct wy_triplet: */
struct triplet_list {
    TAILQ_HEAD(, wy_triplet) head;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
};

#define TRIPLET_LIST_INITIALIZER(t)					\
    { TAILQ_HEAD_INITIALIZER(t.head),					\
	    PTHREAD_MUTEX_INITIALIZER,					\
	    PTHREAD_COND_INITIALIZER }

/* Two such lists are maintained.  The pending list is ordered so that
   prev points to a triplet older than this one, and next points to a
   newer triplet.  Its head member points to the oldest triplet available. */
static struct triplet_list triplet_pending_list =
    TRIPLET_LIST_INITIALIZER(triplet_pending_list);

/* The running list contains triplets which are processed by running
   threads */
static struct triplet_list triplet_running_list =
    TRIPLET_LIST_INITIALIZER(triplet_running_list);

/* Functions for building the ordered doubly-linked list of triplets */

static inline void
triplet_list_lock(struct triplet_list *list)
{
    if (list)
	pthread_mutex_lock(&list->mutex);
}

static inline void
triplet_list_unlock(struct triplet_list *list)
{
    if (list)
	pthread_mutex_unlock(&list->mutex);
}

void
triplet_list_unlink(struct triplet_list *list, struct wy_triplet *tp)
{
    if (list) {
	int head_changed = tp == TAILQ_FIRST(&list->head);
	TAILQ_REMOVE(&list->head, tp, link);
	if (head_changed)
	    pthread_cond_broadcast(&list->cond);
	tp->list = NULL;
    }
}

static void
triplet_list_insert(struct triplet_list *list,
		    struct wy_triplet *newp, struct wy_triplet *anchor,
		    int after)
{
    int head_changed = TAILQ_EMPTY(&list->head);

    if (after) {
	if (!anchor)
	    TAILQ_INSERT_TAIL(&list->head, newp, link);
	else
	    TAILQ_INSERT_AFTER(&list->head, anchor, newp, link);
    } else {
	if (!anchor) {
	    head_changed = 1;
	    TAILQ_INSERT_HEAD(&list->head, newp, link);
	} else {
	    head_changed = anchor == TAILQ_FIRST(&list->head);
	    TAILQ_INSERT_BEFORE(anchor, newp, link);
	}
    }

    newp->list = list;
    if (head_changed)
	pthread_cond_broadcast(&list->cond);
}


static time_t
triplet_timestamp(struct wy_triplet *tp)
{
    int i;
    time_t t = 0;

    if (!tp)
	return 0;

    for (i = 0; i < FILE_TYPE_COUNT; i++) {
	if (tp->file[i].name && (t == 0 || t > tp->file[i].sb.st_mtime))
	    t = tp->file[i].sb.st_mtime;
    }
    return t;
}

static struct timespec *
triplet_ttl(struct wy_triplet *tp, struct timespec *ts)
{
    time_t t;

    if (!tp)
	return NULL;
    t = time(NULL) - triplet_timestamp(tp);
    if (t < tp->spool->file_sweep_time)
	ts->tv_sec += tp->spool->file_sweep_time - t;
    else
	ts->tv_sec = 0;
    ts->tv_nsec = 0;
    return ts;
}


void
triplet_list_ordered_insert(struct triplet_list *list, struct wy_triplet *tp)
{
    time_t t = triplet_timestamp(tp);
    struct wy_triplet *p;

    TAILQ_FOREACH(p, &list->head, link) {
	if (triplet_timestamp(p) >= t)
	    break;
    }
    if (p)
	triplet_list_insert(list, tp, p, 0);
    else
	triplet_list_insert(list, tp, NULL, 1);
}

static struct wy_user *
wy_user_create(struct wy_user *src)
{
    struct wy_user *p = grecs_malloc(sizeof(*p));
    p->next = NULL;
    p->name = src->name;
    p->realname = src->realname;
    p->gpg_key = src->gpg_key;
    p->email = src->email;
    p->fpr = NULL;
    return p;
}

static void
wy_userlist_free(struct wy_user *wp)
{
    while (wp) {
	struct wy_user *next = wp->next;
	free(wp->fpr);
	free(wp);
	wp = next;
    }
}

/* Functions for operation on a symtab of triplets. */
static unsigned
hash_triplet_hasher(void *data, unsigned long n_buckets)
{
    struct wy_triplet const *t = data;
    return grecs_hash_string(t->name, n_buckets);
}

/* Compare two strings for equality.  */
static int
hash_triplet_compare(void const *data1, void const *data2)
{
    struct wy_triplet const *t1 = data1;
    struct wy_triplet const *t2 = data2;
    return t1->spool != t2->spool || strcmp(t1->name, t2->name);
}

/* Reclaim memory storage associated with a table entry */
void
hash_triplet_free(void *data)
{
    int i;
    struct wy_triplet *tp = data;

    free(tp->name);

    for (i = 0; i < FILE_TYPE_COUNT; i++) {
	if (tp->file[i].name)
	    free(tp->file[i].name);
    }

    pthread_mutex_destroy(&tp->in_proc_mutex);
    free(tp->directive);
    free(tp->blurb);
    free(tp->tmp);
    grecs_txtacc_free(tp->acc);
    grecs_txtacc_free(tp->report_acc);

    /* Free uploader and admin lists */
    wy_userlist_free(tp->uploader_list);
    wy_userlist_free(tp->admin_list);

    free(tp);
}

int
triplet_is_in_processing(struct wy_triplet *tp)
{
    int ret;
    pthread_mutex_lock(&tp->in_proc_mutex);
    ret = tp->in_processing;
    pthread_mutex_unlock(&tp->in_proc_mutex);
    return ret;
}

void
triplet_set_in_processing(struct wy_triplet *tp, int val)
{
    pthread_mutex_lock(&tp->in_proc_mutex);
    tp->in_processing = val;
    pthread_mutex_unlock(&tp->in_proc_mutex);
}

char *
triplet_strdup(struct wy_triplet *tp, const char *str)
{
    size_t len = strlen(str);
    grecs_txtacc_grow(tp->acc, str, len + 1);
    return grecs_txtacc_finish(tp->acc, 0);
}

/* Register a file in the triplet table */
struct wy_triplet *
register_file(struct file_info *finfo, struct spool *spool)
{
    struct wy_triplet key, *ret;
    int install = 1;

    pthread_mutex_lock(&triplet_table_mutex);
    if (!triplet_table) {
	triplet_table =
	    grecs_symtab_create(sizeof(struct wy_triplet),
				hash_triplet_hasher,
				hash_triplet_compare,
				NULL, NULL, hash_triplet_free);
	if (!triplet_table)
	    grecs_alloc_die();
    }

    key.name = grecs_malloc(finfo->root_len + 1);
    memcpy(key.name, finfo->name, finfo->root_len);
    key.name[finfo->root_len] = 0;
    key.spool = spool;

    ret = grecs_symtab_lookup_or_install(triplet_table, &key, &install);
    if (ret) {
	if (install) {
	    ret->directive_verified = DIRECTIVE_UNCHECKED;
	    ret->spool = spool;
	    ret->acc = grecs_txtacc_create();
	    ret->report_acc = grecs_txtacc_create();
	    pthread_mutex_init(&ret->in_proc_mutex, NULL);
	}
	ret->file[finfo->type] = *finfo;
    }
    pthread_mutex_unlock(&triplet_table_mutex);

    if (!ret)
	grecs_alloc_die();
    free(key.name);

    triplet_list_lock(&triplet_pending_list);
    if (!install)
	triplet_list_unlink(ret->list, ret);
    triplet_list_ordered_insert(&triplet_pending_list, ret);
    triplet_list_unlock(&triplet_pending_list);
    return ret;
}

struct wy_triplet *
triplet_lookup(struct spool *spool, const char *name)
{
    struct wy_triplet key, *ret;
    struct file_info finfo;

    if (!triplet_table)
	return NULL;

    parse_file_name(name, &finfo);

    key.name = grecs_malloc(finfo.root_len + 1);
    memcpy(key.name, finfo.name, finfo.root_len);
    key.name[finfo.root_len] = 0;
    key.spool = spool;
    file_info_cleanup(&finfo);

    pthread_mutex_lock(&triplet_table_mutex);
    ret = grecs_symtab_lookup_or_install(triplet_table, &key, NULL);
    pthread_mutex_unlock(&triplet_table_mutex);
    free(key.name);

    return ret;
}

enum triplet_state {
    triplet_directive,		/* Short triplet: only a directive is present,
				   but nothing more is required */
    triplet_complete,		/* A complete triplet: all three files are
				   present and have the same owner */
    triplet_incomplete,		/* Incomplete triplet: some files are
				   missing */
    triplet_bad,		/* Bad triplet. Should be removed
				   immediately. */
};

static enum triplet_state
check_triplet_state(struct wy_triplet *trp)
{
    if (trp->file[file_directive].name) {
	if (verify_directive_file(trp) != DIRECTIVE_GOOD)
	    return triplet_bad;

	if (trp->file[file_dist].name == 0 &&
	    trp->file[file_signature].name == 0) {
	    if (directive_get_value(trp, "filename", NULL))
		return triplet_directive;
	} else if (trp->file[file_dist].name &&
		   trp->file[file_signature].name) {
	    if (trp->file[file_dist].sb.st_uid ==
		trp->file[file_signature].sb.st_uid &&
		trp->file[file_dist].sb.st_uid ==
		trp->file[file_directive].sb.st_uid)
		return triplet_complete;
	    else {
		wy_debug(1, (_("%s: invalid triplet: "
			       "UIDs differ"), trp->name));
		return triplet_bad;
	    }
	}
    }

    return triplet_incomplete;
}

void
triplet_enqueue(struct wy_triplet *trp)
{
    pthread_t tid;
    pthread_attr_t attr;

    if (!trp)
	return;
    if (spool_open_dictionaries(trp->spool) == 0) {
	triplet_set_in_processing(trp, 1);
	switch (check_triplet_state(trp)) {
	case triplet_directive:
	case triplet_complete:
	    triplet_list_lock(&triplet_pending_list);
	    triplet_list_unlink(&triplet_pending_list, trp);
	    triplet_list_unlock(&triplet_pending_list);

	    triplet_list_lock(&triplet_running_list);
	    triplet_list_insert(&triplet_running_list, trp, NULL, 1);
	    triplet_list_unlock(&triplet_running_list);

	    pthread_attr_init(&attr);
	    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	    pthread_create(&tid, &attr, wy_thr_triplet, trp);
	    pthread_attr_destroy(&attr);
	    break;
	case triplet_incomplete:
	    triplet_set_in_processing(trp, 0);
	    break;
	case triplet_bad:
	    triplet_set_in_processing(trp, 0);
	    remove_triplet(trp);
	}
    }
}

/* Unlink all parts of the triplet TRP */
static void
remove_triplet_unlocked(struct wy_triplet *trp)
{
    int i;

    triplet_set_in_processing(trp, 1);
    
    triplet_list_unlink(trp->list, trp);

    for (i = 0; i < FILE_TYPE_COUNT; i++) {
	if (trp->file[i].name) {
	    if (!wy_dry_run) {
		if (unlinkat(trp->spool->source_fd, trp->file[i].name, 0)) {
		    if (errno != ENOENT) {
			wydawca_stat_incr(WY_STAT_ERRORS);
			wy_log(LOG_ERR,
			       _("cannot remove %s/%s: %s"),
			       trp->spool->source_dir,
			       trp->file[i].name, strerror(errno));
		    }
		} else
		    wy_log(LOG_NOTICE, _("removing %s/%s"),
			   trp->spool->source_dir, trp->file[i].name);
	    }
	}
    }

    triplet_gpgme_ctx_release(trp);

    pthread_mutex_lock(&triplet_table_mutex);
    grecs_symtab_remove(triplet_table, trp);
    pthread_mutex_unlock(&triplet_table_mutex);
}

void
remove_triplet(struct wy_triplet *trp)
{
    struct triplet_list *list = trp->list;

    triplet_list_lock(list);
    remove_triplet_unlocked(trp);
    triplet_list_unlock(list);
}

void
triplet_commit(struct wy_triplet *trp)
{
    if (spool_open_dictionaries(trp->spool) == 0) {
	wy_debug(1, (_("processing triplet `%s'"), trp->name));
	if (process_directives(trp))
	    wydawca_stat_incr(WY_STAT_ERRORS);
    }
}

void
triplet_remove_file(struct spool *spool, const char *name)
{
    struct wy_triplet *tp = triplet_lookup(spool, name);
    int i, n = 0;

    if (!tp || triplet_is_in_processing(tp))
	return;

    for (i = 0; i < FILE_TYPE_COUNT; i++) {
	if (!tp->file[i].name)
	    /* nothing */ ;
	else if (strcmp(tp->file[i].name, name) == 0)
	    file_info_cleanup(&tp->file[i]);
	else
	    n++;
    }

    if (!n) {
	wy_debug(1, ("deleting empty triplet (%s/%s)",
		     spool->source_dir, name));
	remove_triplet(tp);
    }
}

/* Return true if any part of the triplet TRP was modified more than
   TTL seconds ago */
static int
triplet_expired_p(struct wy_triplet *trp)
{
    int i;
    time_t now;
    time_t ttl;

    if (!trp)
	return 0;

    now = time(NULL);
    ttl = trp->spool->file_sweep_time;

    for (i = 0; i < FILE_TYPE_COUNT; i++) {
	if (trp->file[i].name && (now - trp->file[i].sb.st_mtime) >= ttl) {
	    wy_debug(1, (_("file %s expired"), trp->file[i].name));
	    return 1;
	}
    }
    return 0;
}

void *
wy_thr_cleaner(void *ptr)
{
    wy_set_cur_thread_name("WY_tricleaner");
    triplet_list_lock(&triplet_pending_list);
    while (1) {
	if (!TAILQ_EMPTY(&triplet_pending_list.head)) {
	    struct timespec ts;
	    clock_gettime(CLOCK_REALTIME, &ts);
	    triplet_ttl(TAILQ_FIRST(&triplet_pending_list.head), &ts);
	    pthread_cond_timedwait(&triplet_pending_list.cond,
				   &triplet_pending_list.mutex, &ts);
	} else
	    pthread_cond_wait(&triplet_pending_list.cond,
			      &triplet_pending_list.mutex);
	if (triplet_expired_p(TAILQ_FIRST(&triplet_pending_list.head)))
	    remove_triplet_unlocked(TAILQ_FIRST(&triplet_pending_list.head));
    }
}

void
wy_triplet_wait(void)
{
    size_t n;

    triplet_list_lock(&triplet_running_list);
    while (TAILQ_FIRST(&triplet_running_list.head)) {
	pthread_cond_wait(&triplet_running_list.cond,
			  &triplet_running_list.mutex);
    }
    triplet_list_unlock(&triplet_running_list);

    pthread_mutex_lock(&triplet_table_mutex);
    n = triplet_table ? grecs_symtab_count(triplet_table) : 0;
    pthread_mutex_unlock(&triplet_table_mutex);
    wydawca_stat_add(WY_STAT_INCOMPLETE_TRIPLETS, n);
}

void *
wy_thr_triplet(void *ptr)
{
    struct wy_triplet *trp = ptr;
    wy_set_cur_thread_name("WY_triplet");
    triplet_commit(trp);
    remove_triplet(trp);
    return NULL;
}

/* FIXME */
#define WY_EXP_DFL      0
#define WY_EXP_STATS    0x1

struct wy_varexp {
    int flags;
    struct wy_triplet *triplet;
    struct wy_vardef **def;
    struct dictionary *dict;
    void *handle;
};

static void wy_ws_error(const char *fmt, ...);

static inline void
report_failed_string(char const *str, size_t len)
{
    wydawca_stat_incr(WY_STAT_ERRORS);
    if (len > 32)
	wy_log(LOG_ERR, _("failed to expand string %16.16s...%16.16s"),
	       str, str + len - 16);
    else
	wy_log(LOG_ERR, _("failed to expand string %*.*s"),
	       (int) len, (int) len, str);
}

static int
wy_varexp_lookup_triplet(char **ret, const char *var, size_t len,
			 struct wy_vardef **def,
			 struct wy_triplet *triplet)
{
    int i, j;

    if (def) {
	for (i = 0; def[i]; i++) {
	    for (j = 0; def[i][j].name; j++) {
		if (strlen(def[i][j].name) == len
		    && memcmp(def[i][j].name, var, len) == 0) {
		    return def[i][j].expand(ret, triplet);
		}
	    }
	}
    }
    return WRDSE_UNDEF;
}

static void
trimnl(char *s)
{
    if (s) {
	size_t len = strlen(s);
	while (len > 0 && s[len - 1] == '\n')
	    s[--len] = 0;
    }
}

static int
wy_varexp_lookup(char **retval, const char *var, size_t len, void *clos)
{
    struct wy_varexp *exp = clos;
    int rc;
    char *res;

    rc = wy_varexp_lookup_triplet(&res, var, len, exp->def, exp->triplet);
    if (rc != WRDSE_OK && (exp->flags & WY_EXP_STATS))
	rc = wy_stat_expansion(&res, var, len);
    if (rc == WRDSE_OK) {
	trimnl(res);
	if (exp->dict) {
	    /* FIXME: Revise return code */
	    dictionary_quote_string(exp->dict,
				    exp->handle, res, retval, NULL);
	} else
	    *retval = res;
    }
    return rc;
}

static void
wy_ws_error(const char *fmt, ...)
{
    va_list ap;
    va_start(ap, fmt);
    wy_vlog(LOG_ERR, fmt, ap);
    va_end(ap);
}

char *
wy_expand_string(char const *str, struct wy_varexp *varexp)
{
    struct wordsplit ws;
    char *retval;
    char *lenv[3];
    time_t t;

    lenv[0] = "date";
    time(&t);
    lenv[1] = strdup(ctime(&t));
    lenv[2] = NULL;

    ws.ws_namechar = ":";
    ws.ws_options = WRDSO_NAMECHAR;
    ws.ws_closure = varexp;
    ws.ws_getvar = wy_varexp_lookup;
    ws.ws_error = wy_ws_error;
    ws.ws_env = (const char **) lenv;

    if (wordsplit(str, &ws,
		  WRDSF_OPTIONS
		  | WRDSF_ERROR
		  | WRDSF_ENV
		  | WRDSF_ENV_KV
		  | WRDSF_GETVAR
		  | WRDSF_CLOSURE
		  | WRDSF_NOCMD
		  | WRDSF_UNDEF
		  | WRDSF_NOSPLIT | WRDSF_ERROR | WRDSF_SHOWERR) == 0) {
	retval = ws.ws_wordv[0];
	ws.ws_wordv[0] = NULL;
    } else {
	report_failed_string(str, strlen(str));
	retval = NULL;
    }
    wordsplit_free(&ws);

    return retval;
}

int
wy_expand_copy(char **ret, char const *s)
{
    if (s) {
	char *copy = strdup(s);
	if (!copy)
	    return WRDSE_NOSPACE;
	*ret = copy;
	return WRDSE_OK;
    }
    return WRDSE_UNDEF;
}

#define DECL_TRIPLET_EXP_NAME(name)		\
wy_s_cat2(expand_,name)

#define DECL_TRIPLET_EXP(name, member)					\
static int								\
DECL_TRIPLET_EXP_NAME(name)(char **ret, struct wy_triplet *trp)		\
{									\
    if (trp)								\
	    return wy_expand_copy(ret, trp->member);			\
    return WRDSE_UNDEF;							\
}

#define DECL_TRIPLET_EXP2(name, member, submember)			\
static int								\
DECL_TRIPLET_EXP_NAME(name)(char **ret, struct wy_triplet *trp)		\
{									\
    if (trp) {								\
        if (trp->member)						\
	    return wy_expand_copy(ret, trp->member->submember);	        \
    }									\
    return WRDSE_UNDEF;							\
}

DECL_TRIPLET_EXP2(user, uploader, name)
DECL_TRIPLET_EXP(project, project)
DECL_TRIPLET_EXP(url, spool->url)
DECL_TRIPLET_EXP(spool, spool->tag)
DECL_TRIPLET_EXP(dest_dir, spool->dest_dir)
DECL_TRIPLET_EXP(source_dir, spool->source_dir)

static int
expand_relative_dir(char **ret, struct wy_triplet *trp)
{
    if (trp)
	return wy_expand_copy(ret, trp->relative_dir);
    return WRDSE_UNDEF;
}

static int
expand_comment(char **ret, struct wy_triplet *trp)
{
    if (trp) {
	char const *dir;
	if (directive_get_value(trp, "comment", &dir) == 0)
	    return wy_expand_copy(ret, dir);
    }
    return WRDSE_UNDEF;
}

static void
decode_file_mode(mode_t mode, char *string)
{
    *string++ = mode & S_IRUSR ? 'r' : '-';
    *string++ = mode & S_IWUSR ? 'w' : '-';
    *string++ = (mode & S_ISUID ? (mode & S_IXUSR ? 's' : 'S')
		 : (mode & S_IXUSR ? 'x' : '-'));
    *string++ = mode & S_IRGRP ? 'r' : '-';
    *string++ = mode & S_IWGRP ? 'w' : '-';
    *string++ = (mode & S_ISGID ? (mode & S_IXGRP ? 's' : 'S')
		 : (mode & S_IXGRP ? 'x' : '-'));
    *string++ = mode & S_IROTH ? 'r' : '-';
    *string++ = mode & S_IWOTH ? 'w' : '-';
    *string++ = (mode & S_ISVTX ? (mode & S_IXOTH ? 't' : 'T')
		 : (mode & S_IXOTH ? 'x' : '-'));
    *string = '\0';
}

/* Width of "user/group size", with initial value chosen
   heuristically.  This grows as needed, though this may cause some
   stairstepping in the output.  Make it too small and the output will
   almost always look ragged.  Make it too large and the output will
   be spaced out too far.  */
static int ugswidth = 19;

static int
format_file_data(struct wy_triplet *trp, enum file_type type, char **pret)
{
    char modes[11];
    struct file_info const *info = trp->file + type;
    char timebuf[sizeof "YYYY-MM-DD HH:MM:SS +0000"];
    struct passwd *pw;
    struct group *grp;
    char *sptr = NULL;
    size_t slen = 0;
    int pad;
    char *user_name;
    char *group_name;
    struct tm *tm;
    char *buf = NULL;
    size_t size = 0;

    if (!info->name)
	return 1;

    /* MODE OWNER GROUP SIZE MTIME FILE_NAME MD5SUM? */

    modes[0] = '-';		/* Only regular files are allowed */
    decode_file_mode(info->sb.st_mode, modes + 1);

    /* File time */
    tm = localtime(&info->sb.st_mtime);
    strftime(timebuf, sizeof timebuf, "%Y-%m-%d %H:%M:%S %z", tm);

    pw = getpwuid(TRIPLET_UID(trp));
    if (!pw)
	user_name = "unknown";
    else
	user_name = pw->pw_name;

    grp = getgrgid(TRIPLET_GID(trp));
    if (!grp)
	group_name = "unknown";	/* should not happen */
    else
	group_name = grp->gr_name;

    /* Size */
    if (grecs_asprintf(&sptr, &slen, "%lu",
		       (unsigned long) info->sb.st_size))
	grecs_alloc_die();

    /* Figure out padding and format the buffer */
    slen = strlen(sptr);
    pad = strlen(user_name) + 1 + strlen(group_name) + 1 + slen;
    if (pad > ugswidth)
	ugswidth = pad;

    if (grecs_asprintf(&buf, &size,
		       "%s %s %s %*s %s %s",
		       modes, user_name, group_name, ugswidth - pad + slen,
		       sptr, timebuf, info->name))
	grecs_alloc_die();
    free(sptr);
    *pret = buf;
    return 0;
}

static int
expand_triplet_ls_full(char **ret, struct wy_triplet *trp)
{
    char *buf[FILE_TYPE_COUNT] = { NULL, NULL, NULL };
    size_t size = 0;
    char *value;

    if (!trp)
	return WRDSE_UNDEF;

    if (format_file_data(trp, file_dist, &buf[file_dist]) == 0)
	size += strlen(buf[file_dist]) + 1;
    if (format_file_data(trp, file_signature, &buf[file_signature]) == 0)
	size += strlen(buf[file_signature]) + 1;
    if (format_file_data(trp, file_directive, &buf[file_directive]) == 0)
	size += strlen(buf[file_directive]) + 1;

    value = malloc(size + 1);
    if (!value)
	return WRDSE_NOSPACE;
    value[0] = 0;
    if (buf[file_dist]) {
	strcat(value, buf[file_dist]);
	strcat(value, "\n");
    }
    if (buf[file_signature]) {
	strcat(value, buf[file_signature]);
	strcat(value, "\n");
    }
    if (buf[file_directive]) {
	strcat(value, buf[file_directive]);
	strcat(value, "\n");
    }
    free(buf[file_dist]);
    free(buf[file_signature]);
    free(buf[file_directive]);

    *ret = value;
    return WRDSE_OK;
}

static int
expand_triplet_ls_upload(char **ret, struct wy_triplet *trp)
{
    char *buf[2] = { NULL, NULL };
    size_t size = 0;
    char *value;

    if (!trp)
	return WRDSE_UNDEF;

    if (format_file_data(trp, file_dist, &buf[file_dist]) == 0)
	size += strlen(buf[file_dist]) + 1;
    if (format_file_data(trp, file_signature, &buf[file_signature]) == 0)
	size += strlen(buf[file_signature]) + 1;

    value = malloc(size + 1);
    value[0] = 0;
    if (buf[file_dist]) {
	strcat(value, buf[file_dist]);
	strcat(value, "\n");
    }
    if (buf[file_signature]) {
	strcat(value, buf[file_signature]);
	strcat(value, "\n");
    }

    free(buf[file_dist]);
    free(buf[file_signature]);

    *ret = value;

    return WRDSE_OK;
}

DECL_TRIPLET_EXP(triplet_dist, file[file_dist].name)
DECL_TRIPLET_EXP(triplet_sig, file[file_signature].name)
DECL_TRIPLET_EXP(triplet_dir, file[file_directive].name)

static int
expand_triplet_ls_dist(char **ret, struct wy_triplet *trp)
{
    if (!trp)
	return WRDSE_UNDEF;
    return format_file_data(trp, file_dist, ret);
}

static int
expand_triplet_ls_sig(char **ret, struct wy_triplet *trp)
{
    if (!trp)
	return WRDSE_UNDEF;
    return format_file_data(trp, file_signature, ret);
}

static int
expand_triplet_ls_dir(char **ret, struct wy_triplet *trp)
{
    if (!trp)
	return WRDSE_UNDEF;
    return format_file_data(trp, file_directive, ret);
}

DECL_TRIPLET_EXP2(real_name, uploader, realname)
DECL_TRIPLET_EXP2(user_email, uploader, email)

static int
expand_email_user(char **ret, struct wy_triplet *trp)
{
    if (trp && trp->uploader) {
	size_t size = 0;
	*ret = NULL;
	if (grecs_asprintf(ret, &size, "\"%s\" <%s>",
			   trp->uploader->realname, trp->uploader->email))
	    return WRDSE_NOSPACE;
	return WRDSE_OK;
    }
    return WRDSE_UNDEF;
}

static int
expand_report(char **ret, struct wy_triplet *trp)
{
    return wy_expand_copy(ret, trp->report_str);
}

DECL_TRIPLET_EXP(check_diagn, check_diag);

static int
expand_check_result(char **ret, struct wy_triplet *trp)
{
    int status;
    size_t size = 0;

    if (!trp)
	return WRDSE_UNDEF;

    status = trp->check_result;

    *ret = NULL;
    if (status == 0) {
	*ret = strdup("0");
	if (!*ret)
	    return WRDSE_NOSPACE;
    } else if (WIFEXITED(status)) {
	if (grecs_asprintf(ret, &size, "%d", WEXITSTATUS(status)))
	    return WRDSE_NOSPACE;
    } else if (WIFSIGNALED(status)) {
	if (grecs_asprintf(ret, &size, "SIG+%d", WTERMSIG(status)))
	    return WRDSE_NOSPACE;
    } else if (grecs_asprintf(ret, &size, "[unrecognized return code %d]",
			      status))
	return WRDSE_NOSPACE;
    return WRDSE_OK;
}

struct wy_vardef triplet_query_def[] = {
    { "user", DECL_TRIPLET_EXP_NAME(user) },
    { "user:name", DECL_TRIPLET_EXP_NAME(user) },
    { "project", DECL_TRIPLET_EXP_NAME(project) },
    { "url", DECL_TRIPLET_EXP_NAME(url) },
    { "spool", DECL_TRIPLET_EXP_NAME(spool) },
    { "dir", expand_relative_dir },
    { "dest_dir", DECL_TRIPLET_EXP_NAME(dest_dir) },
    { "source_dir", DECL_TRIPLET_EXP_NAME(source_dir) },
    { "comment", DECL_TRIPLET_EXP_NAME(comment) },
    { NULL }
};

char *
triplet_expand_dictionary_query(struct dictionary *dict, void *handle,
				struct wy_triplet *trp)
{
    struct wy_vardef *vd[2] = { triplet_query_def, NULL };
    struct wy_varexp varexp = {
	.flags = WY_EXP_DFL,
	.triplet = trp,
	.dict = dict,
	.handle = handle,
	.def = vd
    };
    return wy_expand_string(dict->query, &varexp);
}

struct wy_vardef triplet_def[] = {
    { "triplet:dist", DECL_TRIPLET_EXP_NAME(triplet_dist) },
    { "triplet:sig", DECL_TRIPLET_EXP_NAME(triplet_sig) },
    { "triplet:dir", DECL_TRIPLET_EXP_NAME(triplet_dir) },
    { "triplet:ls:full", expand_triplet_ls_full },
    { "triplet:ls:upload", expand_triplet_ls_upload },
    { "triplet:ls:dist", expand_triplet_ls_dist },
    { "triplet:ls:sig", expand_triplet_ls_sig },
    { "triplet:ls:dir", expand_triplet_ls_dir },
    { "check:result", expand_check_result },
    { "check:diagn", DECL_TRIPLET_EXP_NAME(check_diagn) },

    { "user:real_name", DECL_TRIPLET_EXP_NAME(real_name) },

    { "user:email", DECL_TRIPLET_EXP_NAME(user_email) },
    { "email:user", expand_email_user },
    { "report", expand_report },
    { NULL}
};

char *
wy_triplet_expand_param(const char *tmpl, struct wy_triplet *trp,
			struct wy_vardef *extra)
{
    struct wy_vardef *vd[] =
	{ triplet_query_def, triplet_def, extra, NULL };
    struct wy_varexp varexp = {
	.flags = WY_EXP_DFL,
	.triplet = trp,
	.def = vd,
    };
    return wy_expand_string(tmpl, &varexp);
}

char *
wy_expand_stats(const char *tmpl)
{
    struct wy_varexp varexp = {
	.flags = WY_EXP_STATS,
    };
    return wy_expand_string(tmpl, &varexp);
}

struct wy_user *
wy_triplet_get_uploaders(struct wy_triplet *trp)
{
    const struct spool *spool;
    struct dictionary *dict;
    void *md;
    char *command;
    int rc;
    size_t nrows, ncols, i;
    struct wy_user *head, *tail;

    if (trp->uploader_list)
	return trp->uploader_list;

    spool = trp->spool;
    dict = spool->dictionary[project_uploader_dict];

    md = dictionary_open(dict);
    if (!md)
	return NULL;

    command = triplet_expand_dictionary_query(dict, md, trp);

    rc = dictionary_lookup(dict, md, command);
    free(command);
    if (rc) {
	wydawca_stat_incr(WY_STAT_ERRORS);
	wy_log(LOG_ERR, _("cannot get uploaders for %s"), trp->name);
	dictionary_close(dict, md);
	return NULL;
    }

    nrows = dictionary_num_rows(dict);
    if (nrows == 0) {
	wydawca_stat_incr(WY_STAT_ERRORS);
	wy_log(LOG_ERR, _("found no uploaders for %s"), trp->name);
	dictionary_close(dict, md);
	return NULL;
    }

    ncols = dictionary_num_cols(dict);
    if (ncols < 4) {
	wydawca_stat_incr(WY_STAT_ERRORS);
	wy_log(LOG_ERR,
	       _("project-uploader dictionary error: "
		 "too few columns (%lu)"), (unsigned long) ncols);
	dictionary_close(dict, md);
	return NULL;
    }

    head = tail = NULL;
    for (i = 0; i < nrows; i++) {
	const char *p;
	struct wy_user info, *ptr;

	memset(&info, 0, sizeof(info));
	p = dictionary_result(dict, md, i, 0);
	if (p)
	    info.name = triplet_strdup(trp, p);
	p = dictionary_result(dict, md, i, 1);
	if (p)
	    info.realname = triplet_strdup(trp, p);
	p = dictionary_result(dict, md, i, 2);
	if (p)
	    info.email = triplet_strdup(trp, p);
	p = dictionary_result(dict, md, i, 3);
	if (p)
	    info.gpg_key = triplet_strdup(trp, p);

	if (wy_debug_level > 3) {
	    wy_log(LOG_DEBUG, _("name: %s"), SP(info.name));
	    wy_log(LOG_DEBUG, _("realname: %s"), SP(info.realname));
	    wy_log(LOG_DEBUG, _("gpg-key: %s"), SP(info.gpg_key));
	    wy_log(LOG_DEBUG, _("email: %s"), SP(info.email));
	}

	if (!info.name || !info.realname || !info.gpg_key || !info.email) {
	    wydawca_stat_incr(WY_STAT_ERRORS);
	    wy_log(LOG_ERR,
		   _("project-uploader dictionary error: "
		     "malformed row %lu"), (unsigned long) i);
	    /* FIXME: Memory allocated for `info' will
	       be reclaimed only when the triplet is
	       freed. */
	    continue;
	}

	ptr = wy_user_create(&info);
	if (tail)
	    tail->next = ptr;
	else
	    head = ptr;
	tail = ptr;
    }

    dictionary_close(dict, md);

    if (!head) {
	wydawca_stat_incr(WY_STAT_ERRORS);
	wy_log(LOG_ERR, _("no valid uploaders found for %s"), trp->name);
	return NULL;
    }

    trp->uploader_list = head;

    return head;
}

struct wy_user *
wy_triplet_get_uploader(struct wy_triplet *trp)
{
    return trp->uploader;
}


struct wy_user *
wy_triplet_get_admins(struct wy_triplet *trp)
{
    const struct spool *spool;
    struct dictionary *dict;
    void *md;
    char *command;
    int rc;
    size_t nrows, ncols, i;
    struct wy_user *head, *tail;

    if (trp->admin_list)
	return trp->admin_list;

    spool = trp->spool;
    dict = spool->dictionary[project_owner_dict];

    if (dict->type == dictionary_none) {
	wydawca_stat_incr(WY_STAT_ERRORS);
	wy_log(LOG_ERR,
	       _("%s: dictionary %s not configured (spool %s)"),
	       trp->name, "project_owner_dict", spool->tag);
	return NULL;
    }

    md = dictionary_open(dict);
    if (!md) {
	wydawca_stat_incr(WY_STAT_ERRORS);
	wy_log(LOG_ERR,
	       _("%s: failed to open dictionary %s (spool %s)"),
	       trp->name, "project_owner_dict", spool->tag);
	return NULL;
    }

    command = triplet_expand_dictionary_query(dict, md, trp);

    rc = dictionary_lookup(dict, md, command);
    free(command);
    if (rc) {
	wydawca_stat_incr(WY_STAT_ERRORS);
	wy_log(LOG_ERR,
	       _("%s: cannot obtain recipient emails"), trp->name);
	dictionary_close(dict, md);
	return NULL;
    }

    nrows = dictionary_num_rows(dict);
    ncols = dictionary_num_cols(dict);

    if (nrows == 0) {
	wydawca_stat_incr(WY_STAT_ERRORS);
	wy_log(LOG_ERR,
	       _("%s: cannot obtain recipient emails"), trp->name);
	return NULL;
    }

    head = tail = NULL;
    for (i = 0; i < nrows; i++) {
	const char *p;
	struct wy_user info, *ptr;

	memset(&info, 0, sizeof(info));
	p = dictionary_result(dict, md, i, 0);
	if (p)
	    info.email = triplet_strdup(trp, p);
	if (ncols > 0 && (p = dictionary_result(dict, md, i, 1)))
	    info.realname = triplet_strdup(trp, p);

	if (wy_debug_level > 3) {
	    wy_log(LOG_DEBUG, _("realname: %s"), SP(info.realname));
	    wy_log(LOG_DEBUG, _("email: %s"), SP(info.email));
	}

	ptr = wy_user_create(&info);
	if (tail)
	    tail->next = ptr;
	else
	    head = ptr;
	tail = ptr;

    }
    dictionary_close(dict, md);
    trp->admin_list = head;

    return trp->admin_list;
}

const char *
wy_triplet_project(struct wy_triplet *trp)
{
    return trp->project;
}

void
triplet_report_add(struct wy_triplet *trp, const char *fmt, ...)
{
    va_list ap;
    char *str = NULL;
    size_t size = 0;

    va_start(ap, fmt);
    grecs_vasprintf(&str, &size, fmt, ap);
    va_end(ap);
    if (str) {
	grecs_txtacc_grow_string(trp->report_acc, str);
	grecs_txtacc_grow_char(trp->report_acc, '\n');
    }
    free(str);
}

void
triplet_report_finish(struct wy_triplet *trp)
{
    if (!trp->report_str) {
	grecs_txtacc_grow_char(trp->report_acc, 0);
	trp->report_str = grecs_txtacc_finish(trp->report_acc, 0);
    }
}
