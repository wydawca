/* wydawca - automatic release submission daemon
   Copyright (C) 2009-2013, 2017, 2019-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program. If not, see <http://www.gnu.org/licenses/>. */

int builtin_init(struct dictionary *);
int builtin_done(struct dictionary *);
void *builtin_open(struct dictionary *);
int builtin_get(struct dictionary *, void *, unsigned, unsigned);
int builtin_lookup(struct dictionary *, void *, const char *);
int builtin_free_result(struct dictionary *, void *);
