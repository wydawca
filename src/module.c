/* wydawca - automatic release submission daemon
   Copyright (C) 2007-2013, 2017, 2019-2023 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include "wydawca.h"
#include <dlfcn.h>

static STAILQ_HEAD(,module) mod_head = STAILQ_HEAD_INITIALIZER(mod_head);
struct grecs_list *module_load_path, *module_prepend_load_path;

static struct module *
modlookup(const char *name)
{
    struct module *p;
    STAILQ_FOREACH(p, &mod_head, link) {
	if (strcmp(p->name, name) == 0)
	    return p;
    }
    return NULL;
}

static struct module *
modinstall(const char *name, const char *path, grecs_locus_t * loc)
{
    struct module *p;

    p = modlookup(name);
    if (p) {
	grecs_error(loc, 0, _("module %s already declared"), name);
	grecs_error(&p->locus, 0, _("previously declared here"));
	return NULL;
    }

    p = grecs_zalloc(sizeof(*p));
    p->name = grecs_strdup(name);
    p->path = grecs_strdup(path);
    p->locus.beg.file = grecs_strdup(loc->beg.file);
    p->locus.beg.line = loc->beg.line;
    p->locus.beg.col = loc->beg.col;
    p->locus.end.file = grecs_strdup(loc->end.file);
    p->locus.end.line = loc->end.line;
    p->locus.end.col = loc->end.col;

    STAILQ_INSERT_TAIL(&mod_head, p, link);

    return p;
}

int
cb_module(enum grecs_callback_command cmd, grecs_node_t *node,
	  void *varptr, void *cb_data)
{
    grecs_value_t *name;
    grecs_value_t *path;
    grecs_locus_t *locus = &node->locus;
    grecs_value_t *value = node->v.value;

    if (cmd != grecs_callback_set_value) {
	grecs_error(locus, 0, _("Unexpected block statement"));
	return 1;
    }

    if (!(name = get_arg(value, 0, GRECS_TYPE_STRING)))
	return 1;
    if (!(path = get_arg(value, 1, GRECS_TYPE_STRING)))
	return 1;

    modinstall(name->v.string, path->v.string, locus);

    return 0;
};

static void *
resolve_sym(struct module *mod, const char *name, int mustbe)
{
    void *sym = dlsym(mod->handle, name);
    if (!sym && mustbe)
	grecs_error(&mod->locus, 0,
		    _("can't find symbol \"%s\" in \"%s\": %s"),
		    name, mod->name, dlerror());
    return sym;
}

static char *
modfind_in_dir(struct module *mod, char const *dir)
{
    char *file = concat_file(dir, mod->path, NULL);
    if (access(file, R_OK)) {
	if (errno != ENOENT)
	    grecs_error(&mod->locus, errno, _("can't access %s"), file);
	free (file);
	file = NULL;
    }
    return file;
}

static char *
modfind_in_list(struct module *mod, struct grecs_list *list)
{
    struct grecs_list_entry *ep;
    char *file = NULL;
    if (list) {
	for (ep = list->head; ep; ep = ep->next) {
	    if ((file = modfind_in_dir(mod, ep->data)) != NULL)
		break;
	}
    }
    return file;
}	

static char *
modfind(struct module *mod)
{
    char *file;

    if (mod->path[0] == '/') {
	if (access(mod->path, R_OK)) {
	    grecs_error(&mod->locus, errno, _("can't access %s"), file);
	    return NULL;
	}
	return grecs_strdup(mod->path);
    }
    
    if ((file = modfind_in_list(mod, module_prepend_load_path)) != NULL)
	return file;
    if ((file = modfind_in_dir(mod, WYDAWCA_MODDIR)) != NULL)
	return file;
    if ((file = modfind_in_list(mod, module_load_path)) != NULL)
	return file;
    return NULL;
}

static int
modload(struct module *mod)
{
    void *handle = NULL;
    char *libname;
    
    if (mod->handle) {
	grecs_error(&mod->locus, 0, _("already loaded"));
	return 0;
    }

    if ((libname = modfind(mod)) == NULL) {
	grecs_error(mod->locus.beg.file ? &mod->locus : NULL, 0,
		    _("cannot find module %s"), mod->path);
	return 0;
    }

    handle = dlopen(libname, RTLD_LAZY | RTLD_LOCAL);
    if (!handle) {
	grecs_error(mod->locus.beg.file ? &mod->locus : NULL, 0,
		    _("cannot load module %s: %s"), libname,
		    dlerror());
	free(libname);
	return 1;
    }
    free(libname);
    
    mod->handle = handle;
    mod->open = resolve_sym(mod, "wy_open", 0);
    mod->close = resolve_sym(mod, "wy_close", 0);

    mod->config = resolve_sym(mod, "wy_config", 1);
    mod->notify = resolve_sym(mod, "wy_notify", 1);
    mod->flush = resolve_sym(mod, "wy_flush", 0);

    if (mod->open) {
	if (mod->open(mod->modinit)) {
	    grecs_error(mod->modinit ? &mod->modinit->locus : NULL,
			0, _("failed to initialize module %s"), mod->name);
	    return 1;
	}
    }
    return 0;
}

static int
conf_notification_modules(NOTIFYQ *nq)
{
    struct notification *np;
    NOTIFYQ_FOREACH(np, nq) {
	if (np->modname) {
	    struct module *mod = modlookup(np->modname);
	    if (!mod) {
		wy_log(LOG_ERR, "%s: no such module", np->modname);
		return 1;
	    }
	    if (!np->modcfg && mod->config && np->modnode) {
		np->modcfg = mod->config(np->modnode);
		if (!np->modcfg) {
		    wy_log(LOG_ERR,
			   "%s: failed to configure", np->modname);
		    return 1;
		}
	    }
	}
    }
    return 0;
}

static int
spoolmodcfg(struct spool *spool, void *unused)
{
    return conf_notification_modules(&spool->notification_queue);
}

void
modules_load()
{
    struct module *mod;

    STAILQ_FOREACH(mod, &mod_head, link) {
	if (modload(mod))
	    exit(EX_UNAVAILABLE);
    }

    if (for_each_spool(spoolmodcfg, NULL)) {
	wy_log(LOG_CRIT, _("some modules failed to configure, exiting"));
	exit(EX_UNAVAILABLE);
    }
    conf_notification_modules(&default_notification);
}

void
modules_close(void)
{
    struct module *mod;

    while ((mod = STAILQ_FIRST(&mod_head)) != NULL) {
	if (mod->handle) {
	    if (mod->close)
		mod->close();
	    dlclose(mod->handle);
	}
	STAILQ_REMOVE_HEAD(&mod_head, link);
    }
}

int
module_set_init(const char *name, grecs_node_t * node)
{
    struct module *mod = modlookup(name);
    if (!mod)
	return 1;
    mod->modinit = node;
    return 0;
}

void
module_notify(const char *name, void *modcfg,
	      enum wy_event ev, struct wy_triplet *trp)
{
    struct module *mod = modlookup(name);

    if (!mod) {
	wy_log(LOG_ERR, "no such module: %s", name);
	return;
    }
    if (mod->notify)
	mod->notify(modcfg, ev, trp);
}

void
module_flush(const char *name, void *modcfg)
{
    struct module *mod = modlookup(name);

    if (!mod) {
	wy_log(LOG_ERR, "no such module: %s", name);
	return;
    }
    if (mod->flush)
	mod->flush(modcfg);
}

void
module_help(const char *modname)
{
    struct module mod;
    void (*help) (void);

    memset(&mod, 0, sizeof(mod));
    mod.path = (char *) modname;

    if (modload(&mod))
	exit(EX_UNAVAILABLE);

    help = resolve_sym(&mod, "wy_help", 0);
    if (!help)
	wy_log(LOG_NOTICE, "no help for %s", modname);
    else
	help();

    dlclose(mod.handle);
    mod.handle = NULL;
}
