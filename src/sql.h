/* wydawca - automatic release submission daemon
   Copyright (C) 2009-2013, 2017, 2019-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <mysql/mysql.h>

struct sqlconn {
    char *ident;
    char *config_file;
    char *config_group;
    char *host;
    char *socket;
    short port;
    char *database;
    char *user;
    char *password;
    char *cacert;
    size_t initcount;		/* Number of initializations */
    MYSQL mysql;
    MYSQL_RES *result;
};

void sql_register_conn(struct sqlconn *);
int sql_connection_exists_p(const char *);
struct sqlconn *sql_find_connection(const char *ident);

int sql_init_dictionary(struct dictionary *dict);
int sql_done_dictionary(struct dictionary *dict);
void *sql_open(struct dictionary *dict);

int sql_lookup_dictionary(struct dictionary *dict, void *handle,
			  const char *cmd);
int sql_get_dictionary(struct dictionary *dict, void *handle,
		       unsigned nrow, unsigned ncol);
int sql_free_result(struct dictionary *dict, void *handle);
int sql_quote(struct dictionary *, void *, const char *, char **,
	      size_t *);
