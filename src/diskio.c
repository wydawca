/* wydawca - automatic release submission daemon
   Copyright (C) 2007-2013, 2017, 2019-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include "wydawca.h"

char *
concat_file(char const *dir, ...)
{
    va_list ap;
    char *arg;
    char *buf = NULL;
    size_t buflen = 0;
    size_t bufmax = 0;

    bufmax = strlen(dir);
    buf = grecs_malloc(bufmax + 1);
    strcpy(buf, dir);
    buf[buflen++] = *dir++;

    while (*dir) {
	if (!(*dir == '/' && buf[buflen-1] == '/'))
	    buf[buflen++] = *dir;
	dir++;
    }

    va_start(ap, dir);
    while ((arg = va_arg(ap, char*)) != NULL) {
	size_t len = strlen(arg);
	if (buflen + len + 2 >= bufmax) {
	    while (buflen + len + 2 >= bufmax) {
		if ((size_t) -1 / 3 * 2 <= bufmax)
		    grecs_alloc_die();
		bufmax += (bufmax + 1) / 2;
	    }
	    buf = grecs_realloc(buf, bufmax);
	}
	if (buf[buflen-1] != '/')
	    buf[buflen++] = '/';
	while (*arg) {
	    if (!(*arg == '/' && buf[buflen-1] == '/'))
		buf[buflen++] = *arg;
	    arg++;
	}
    }
    va_end(ap);
    buf[buflen] = 0;
    return buf;
}

/* Create the directory DIR, eventually creating all intermediate directories
   starting from DIR + BASELEN. */
static int
create_hierarchy_internal(int dirfd, char *dir, size_t baselen)
{
    struct stat st;
    size_t i;

    i = baselen;
    while (1) {
	if (fstatat(dirfd, dir, &st, 0) == 0) {
	    if (!S_ISDIR(st.st_mode)) {
		wy_log(LOG_ERR, _("component %s is not a directory"), dir);
		return -1;
	    }
	    if (i == baselen)
		return 0;
	    break;
	}
	if (errno != ENOENT) {
	    wy_log(LOG_ERR, _("cannot stat file %s: %s"), dir,
		   strerror(errno));
	    return -1;
	}
	while (i > 0 && dir[i-1] != '/')
	    i--;
	if (i == 0)
	    break;
	dir[--i] = 0;
    }

    do {
	if (i)
	    dir[i] = '/';
	if (mkdirat(dirfd, dir, MKDIR_PERMISSIONS)) {
	    wy_log(LOG_ERR, _("cannot create directory %s: %s"),
		   dir, strerror(errno));
	    return -1;
	}
	i += strlen(dir + i);
    } while (i < baselen);

    return 0;
}

int
create_hierarchy(int dirfd, char const *dir)
{
    char *storage;
    int rc;
    size_t baselen = strlen(dir);

    if (wy_dry_run)
	return 0;

    while (baselen > 0 && dir[baselen-1] == '/')
	baselen--;
    if (baselen == 0)
	return 0;

    storage = malloc(baselen + 1);
    if (!storage) {
	wy_log(LOG_ERR, "%s", strerror(errno));
	return -1;
    }
    memcpy(storage, dir, baselen);
    storage[baselen] = 0;
    rc = create_hierarchy_internal(dirfd, storage, baselen);
    free(storage);
    return rc;
}

/* Copy FILE to DST_FILE. */
int
copy_file(int src_fd, const char *src_file, int dst_fd, const char *dst_file)
{
    int in_fd, out_fd;
    struct stat st;
    int rc;
    char *buf;
    size_t bufsize;
    size_t fsize;

    in_fd = openat(src_fd, src_file, O_RDONLY);

    if (in_fd == -1) {
	wy_log(LOG_ERR,
	       _("cannot open source file %s for reading: %s"),
	       src_file, strerror(errno));
	return 1;
    }

    if (fstat(in_fd, &st)) {
	wy_log(LOG_ERR,
	       _("cannot stat source file %s: %s"),
	       src_file, strerror(errno));
	close(in_fd);
	return 1;
    }

    out_fd = openat(dst_fd, dst_file, O_CREAT|O_TRUNC|O_RDWR,
		    CREAT_PERMISSIONS);
    if (out_fd == -1) {
	wy_log(LOG_ERR,
	       _("cannot create destination file %s: %s"),
	       dst_file, strerror(errno));
	close(in_fd);
	return 1;
    }

    buf = NULL;
    fsize = st.st_size;

    for (bufsize = fsize; bufsize > 0 && (buf = malloc(bufsize)) == NULL;
	 bufsize /= 2);
    if (bufsize == 0)
	grecs_alloc_die();

    rc = 0;
    while (fsize > 0) {
	size_t rest;
	size_t rdbytes;

	rest = fsize > bufsize ? bufsize : fsize;
	rdbytes = read(in_fd, buf, rest);
	if (rdbytes == -1) {
	    wy_log(LOG_ERR, _("unexpected error reading %s: %s"),
		   src_file, strerror(errno));
	    rc = 1;
	    break;
	}
	rest = write(out_fd, buf, rdbytes);
	if (rest == -1) {
	    wy_log(LOG_ERR,
		   _("unexpected error writing to %s: %s"),
		   dst_file, strerror(errno));
	    rc = 1;
	    break;
	} else if (rest != rdbytes) {
	    wy_log(LOG_ERR, _("short write on %s"), dst_file);
	    rc = 1;
	}
	fsize -= rdbytes;
    }
    free(buf);

    close(in_fd);
    close(out_fd);
    if (rc)
	unlinkat(dst_fd, dst_file, 0);
    return rc;
}

/* Move FILE to DST_FILE. If they reside on different devices, use copy_file
   + unlink. */
static int
do_move_file(int src_fd, const char *src_file, int dst_fd, const char *dst_file)
{
    int rc = 0;

    if (renameat(src_fd, src_file, dst_fd, dst_file)) {
	if (errno == EXDEV) {
	    if (copy_file(src_fd, src_file, dst_fd, dst_file)) {
		wy_log(LOG_CRIT, _("cannot copy %s to %s: %s"),
		       src_file, dst_file, strerror(errno));
		rc = 1;
	    } else if (unlinkat(src_fd, src_file, 0)) {
		wy_log(LOG_ERR, _("cannot unlink %s: %s"),
		       src_file, strerror(errno));
	    }
	} else {
	    wy_log(LOG_CRIT, _("cannot move %s to %s: %s"),
		   src_file, dst_file, strerror(errno));
	    rc = 1;
	}
    }
    return rc;
}

static int
tar_append_file(char const *archive, struct wy_triplet *trp,
		char const *file_name)
{
    char *full_file_name = NULL;
    const char *argv[6];
    int rc;

    full_file_name = concat_file(trp->spool->dest_dir, trp->relative_dir,
				 file_name, NULL);

    wy_debug(1, (_("tarring %s to %s"), full_file_name, archive));
    if (wy_dry_run)
	rc = 0;
    else {
	argv[0] = tar_command_name;
	argv[1] = "-f";
	argv[2] = archive;
	argv[3] = "-r";
	argv[4] = full_file_name;
	argv[5] = NULL;

	rc = 1;
	switch (wydawca_exec(6, argv, NULL)) {
	case exec_success:
	    rc = 0;
	    break;

	case exec_fail:
	case exec_error:
	    wy_log(LOG_ERR, _("cannot archive %s"), file_name);
	    break;
	}
    }
    free(full_file_name);

    return rc;
}

static int
backup_copy(int pkg_dir_fd, struct wy_triplet *trp, char const *file_name)
{
    const struct archive_descr *archive = &trp->spool->archive;
    int dst_dir_fd;
    int rc;
    struct stat st;
    char *dst_dir;

    if (archive->name[0] == '/') {
	int fd;

	dst_dir = concat_file(archive->name, trp->relative_dir, NULL);

	wy_debug(1, (_("backing up %s/%s/%s to %s"),
		     trp->spool->source_dir, trp->relative_dir, file_name,
		     dst_dir));
	if (wy_dry_run) {
	    free(dst_dir);
	    return 0;
	}

	if (create_hierarchy(AT_FDCWD, archive->name)) {
	    free(dst_dir);
	    return -1;
	}

	fd = open(archive->name, O_SEARCH);
	if (create_hierarchy(fd, trp->relative_dir)) {
	    free(dst_dir);
	    close(fd);
	    return -1;
	}
	dst_dir_fd = openat(fd, trp->relative_dir, O_SEARCH);
	close(fd);
    } else {
	dst_dir = concat_file(trp->spool->dest_dir, trp->relative_dir,
			      archive->name, NULL);

	wy_debug(1, (_("backing up %s/%s/%s to %s"),
		     trp->spool->source_dir, trp->relative_dir, file_name,
		     dst_dir));
	if (wy_dry_run) {
	    free(dst_dir);
	    return 0;
	}
	if (create_hierarchy(pkg_dir_fd, archive->name)) {
	    free(dst_dir);
	    return -1;
	}
	dst_dir_fd = openat(pkg_dir_fd, archive->name, O_SEARCH);
    }
    if (dst_dir_fd == -1) {
	wy_log(LOG_ERR, _("cannot open backup directory %s: %s"),
	       dst_dir, strerror(errno));
	free(dst_dir);
	return -1;
    }

    if (fstatat(dst_dir_fd, file_name, &st, 0) == 0) {
	if (archive->backup_type == no_backups) {
	    wy_debug(1, (_("removing previous archive file %s/%s"),
			 dst_dir, file_name));
	    if (unlinkat(dst_dir_fd, file_name, 0)) {
		wy_log(LOG_ERR,
		       _("cannot unlink previous archive file %s/%s: %s"),
		       dst_dir, file_name, strerror(errno));
		free(dst_dir);
		return -1;
	    }
	} else {
	    char *archive_file_name =
		find_backup_file_name(dst_dir_fd, file_name,
				      archive->backup_type);
	    wy_debug(1,
		     (_("backing up previous archive file %s/%s to %s/%s"),
		      dst_dir, file_name,
		      dst_dir, archive_file_name));

	    rc = do_move_file(dst_dir_fd, file_name,
			      dst_dir_fd, archive_file_name);
	    if (rc) {
		wy_log(LOG_ERR,
		       _("backing up %s/%s to %s/%s failed: %s"),
		       dst_dir, file_name,
		       dst_dir, archive_file_name,
		       strerror(errno));
		free(archive_file_name);
		free(dst_dir);
		return 1;
	    }
	    free(archive_file_name);
	}
    } else if (errno != ENOENT) {
	wy_log(LOG_ERR, _("cannot stat file %s/%s: %s"),
	       dst_dir, file_name, strerror(errno));
	free(dst_dir);
	return -1;
    }
    free(dst_dir);

    rc = do_move_file(pkg_dir_fd, file_name, dst_dir_fd, file_name);
    close(dst_dir_fd);
    return rc;
}

static int
do_archive_file(int pkg_dir_fd, struct wy_triplet *trp, char const *file_name)
{
    const struct archive_descr *archive = &trp->spool->archive;

    switch (archive->type) {
    case archive_none:
	break;

    case archive_directory:
	if (backup_copy(pkg_dir_fd, trp, file_name))
	    return -1;
	wydawca_stat_incr(WY_STAT_ARCHIVES);
	break;

    case archive_tar:
	if (tar_append_file(archive->name, trp, file_name))
	    return 1;
	wydawca_stat_incr(WY_STAT_ARCHIVES);
	break;
    }

    if (!wy_dry_run && unlinkat(pkg_dir_fd, file_name, 0)
	&& errno != ENOENT) {
	wy_log(LOG_ERR, _("canot unlink file %s/%s/%s: %s"),
	       trp->spool->dest_dir, trp->relative_dir,
	       file_name,
	       strerror(errno));
	return 1;
    }
    return 0;
}

static int
triplet_open_dest_dir(struct wy_triplet *trp)
{
    int dest_dir_fd, pkg_dir_fd;

    dest_dir_fd = open(trp->spool->dest_dir, O_SEARCH);
    if (dest_dir_fd == -1) {
	wy_log(LOG_ERR, _("can't open destination directory %s: %s"),
	       trp->spool->dest_dir, strerror(errno));
	return -1;
    }

    if (create_hierarchy(dest_dir_fd, trp->relative_dir)) {
	close(dest_dir_fd);
	return -1;
    }

    pkg_dir_fd = openat(dest_dir_fd, trp->relative_dir, O_SEARCH);
    close(dest_dir_fd);
    if (pkg_dir_fd == -1) {
	wy_log(LOG_ERR, _("can't open package destination directory %s/%s: %s"),
	       trp->spool->dest_dir, trp->relative_dir, strerror(errno));
    }
    return pkg_dir_fd;
}

int
dir_move_file(struct wy_triplet *trp, enum file_type file_id)
{
    int rc = 0;
    const struct spool *spool = trp->spool;
    int pkg_dir_fd;
    struct stat st;

    wy_debug(1, (_("installing %s to %s/%s"),
		 trp->file[file_id].name, spool->dest_dir,
		 trp->relative_dir));
    if (!wy_dry_run) {
	pkg_dir_fd = triplet_open_dest_dir(trp);
	if (pkg_dir_fd == -1)
	    return -1;

	if (fstatat(pkg_dir_fd, trp->file[file_id].name, &st, 0) == 0) {
	    if (trp->replace_allowed) {
		rc = do_archive_file(pkg_dir_fd, trp, trp->file[file_id].name);
	    } else {
		wy_log(LOG_ERR,
		       _("refusing to upload %s because it already "
			 "exists and replace is not allowed"),
		       trp->file[file_id].name);
		rc = -1;
	    }
	}

	if (rc == 0)
	    rc = do_move_file(spool->source_fd, trp->file[file_id].name,
			      pkg_dir_fd, trp->file[file_id].name);
	close(pkg_dir_fd);
    }

    if (rc == 0)
	wydawca_stat_incr(WY_STAT_UPLOADS);
    return rc;
}

static char *
make_signame(const char *file_name)
{
    size_t len;

    if (((len = strlen(file_name)) <= SUF_SIG_LEN
	 || memcmp(file_name + len - SUF_SIG_LEN, SUF_SIG, SUF_SIG_LEN))) {
	char *signame = grecs_malloc(len + SUF_SIG_LEN + 1);
	strcpy(signame, file_name);
	return strcat(signame, SUF_SIG);
    }
    return NULL;
}

/* Archive the file FILE_NAME, located in DPAIR->dest_dir, and remove the
   file. Unless FILE_NAME ends in ".sig", do the same with FILE_NAME.sig,
   if such a file exists.
   Do nothing if wy_dry_run is set.
*/
int
dir_archive_file(struct wy_triplet *trp, const char *file_name)
{
    char *rname, *signame;
    int pkg_dir_fd;
    int rc;
    struct stat st;

    rname = safe_file_name_alloc(file_name);
    if (!rname || rname[0] == '/') {
	wy_log(LOG_ERR,
	       _("refusing to archive: suspicious symlink source %s"),
	       rname);
	free(rname);
	return -1;
    }

    wy_debug(1, (_("archiving %s/%s/%s"),
		 trp->spool->dest_dir, trp->relative_dir, rname));
    if (!wy_dry_run) {
	pkg_dir_fd = triplet_open_dest_dir(trp);
	if (pkg_dir_fd == -1)
	    rc = -1;
	else {
	    if (fstatat(pkg_dir_fd, rname, &st, 0) == 0) {
		rc = do_archive_file(pkg_dir_fd, trp, rname);
		if (rc == 0)
		    wydawca_stat_incr(WY_STAT_ARCHIVES);
	    } else {
		wy_log(LOG_ERR, _("cannot stat file %s/%s/%s: %s"),
		       trp->spool->dest_dir, trp->relative_dir, rname,
		       strerror(errno));
		rc = -1;
	    }

	    if (rc == 0 && (signame = make_signame(rname)) != NULL) {
		if (fstatat(pkg_dir_fd, signame, &st, 0) == 0) {
		    if (archive_signatures) {
			rc = do_archive_file(pkg_dir_fd, trp, signame);
		    } else if (unlinkat(pkg_dir_fd, signame, 0)
			       && errno != ENOENT) {
			wy_log(LOG_ERR, _("canot unlink file %s/%s/%s: %s"),
			       trp->spool->dest_dir, trp->relative_dir,
			       signame,
			       strerror(errno));
			rc = -1;
		    }
		} else if (errno != ENOENT) {
		    wy_log(LOG_ERR, _("cannot stat file %s/%s/%s: %s"),
			   trp->spool->dest_dir, trp->relative_dir, signame,
			   strerror(errno));
		    rc = -1;
		}
		free(signame);
	    }
	    close(pkg_dir_fd);
	}
    }

    free(rname);
    return rc;
}

static int
symlink_triplet_file(int dirfd, struct wy_triplet *trp, const char *src,
		     const char *dst)
{
    struct stat st;
    int rc;

    /* Check source */
    if (fstatat(dirfd, src, &st, 0)) {
	wy_log(LOG_ERR, _("cannot stat file %s/%s/%s: %s"),
	       trp->spool->dest_dir, trp->relative_dir, src,
	       strerror(errno));
	return -1;
    }

    /* Check destination */
    if (fstatat(dirfd, dst, &st, AT_SYMLINK_NOFOLLOW) == 0) {
	if (!S_ISLNK(st.st_mode)) {
	    wy_log(LOG_ERR,
		   _("file %s/%s/%s exists and is not a symbolic link"),
		   trp->spool->dest_dir, trp->relative_dir, dst);
	    return -1;
	} else if (unlinkat(dirfd, dst, 0)) {
	    wy_log(LOG_ERR,
		   _("cannot unlink %s/%s/%s: %s"),
		   trp->spool->dest_dir, trp->relative_dir, dst,
		   strerror(errno));
	    return -1;
	}
    } else if (errno != ENOENT) {
	wy_log(LOG_ERR, _("cannot stat file %s: %s"), dst, strerror(errno));
	return -1;
    }

    rc = symlinkat(src, dirfd, dst);
    if (rc)
	wy_log(LOG_ERR,
	       _("symlinking %s to %s in directory %s/%s failed: %s"),
	       src, dst, trp->spool->dest_dir, trp->relative_dir,
	       strerror(errno));
    else
	wydawca_stat_incr(WY_STAT_SYMLINKS);

    return rc;
}

/* Create a symbolic link from WANTED_SRC to WANTED_DST in the subdirectory
   TRP->relative_dir of SPOOL->dest_dir.

   Do nothing if wy_dry_run is set. */
int
dir_symlink_file(struct wy_triplet *trp,
		 const char *wanted_src, const char *wanted_dst)
{
    char *src, *dst;
    int rc;
    int pkg_dir_fd;

    src = safe_file_name_alloc(wanted_src);
    if (!src || src[0] == '/') {
	wy_log(LOG_ERR,
	       _("refusing to symlink: suspicious symlink source %s"),
	       wanted_src);
	free(src);
	return -1;
    }

    dst = safe_file_name_alloc(wanted_dst);
    if (!dst || dst[0] == '/') {
	wy_log(LOG_ERR,
	       _("refusing to symlink: suspicious symlink destination %s"),
	       wanted_dst);
	free(src);
	free(dst);
	return 1;
    }

    wy_debug(1, (_("symlinking %s to %s in directory %s/%s"),
		 src, dst, trp->spool->dest_dir, trp->relative_dir));
    if (wy_dry_run) {
	free(src);
	free(dst);
	return 0;
    }

    pkg_dir_fd = triplet_open_dest_dir(trp);
    if (pkg_dir_fd == -1)
	return -1;

    rc = symlink_triplet_file(pkg_dir_fd, trp, src, dst);
    if (rc == 0) {
	char *src_sig = make_signame(src);
	char *dst_sig = make_signame(dst);
	if (src_sig && dst_sig) {
	    symlink_triplet_file(pkg_dir_fd, trp, src_sig, dst_sig);
	}
	free(src_sig);
	free(dst_sig);
    }

    close(pkg_dir_fd);
    free(src);
    free(dst);
    return rc;
}

/* Auxiliary function for rmsymlink_file (see below) */
static int
rmsymlink_triplet_file(int dirfd, struct wy_triplet *trp, const char *file)
{
    struct stat st;

    wy_debug(1, (_("removing symbolic link %s/%s/%s"),
		 trp->spool->dest_dir, trp->relative_dir,
		 file));

    if (fstatat(dirfd, file, &st, AT_SYMLINK_NOFOLLOW)) {
	if (errno == ENOENT) {
	    wy_log(LOG_NOTICE,
		   _("symlink %s/%s/%s' does not exist"),
		   trp->spool->dest_dir, trp->relative_dir,
		   file);
	    return 0;
	} else if (!S_ISLNK(st.st_mode)) {
	    wy_log(LOG_ERR,
		   _("refusing to unlink %s/%s/%s: is not a symlink"),
		   trp->spool->dest_dir, trp->relative_dir,
		   file);
	    return -1;
	}
    }

    if (!wy_dry_run && unlinkat(dirfd, file, 0)) {
	wy_log(LOG_ERR, _("cannot unlink %s/%s/%s: %s"),
	       trp->spool->dest_dir, trp->relative_dir,
	       file,
	       strerror(errno));
	return -1;
    }
    wydawca_stat_incr(WY_STAT_RMSYMLINKS);
    return 0;
}

/* Remove the symbolic link TRP->spool->dest_dir/TRP->relative_dir/FILE_NAME

   Do nothing if wy_dry_run is set. */
int
dir_rmsymlink_file(struct wy_triplet *trp, const char *file_name)
{
    char *symlink, *signame;
    int rc;
    int pkg_dir_fd;

    symlink = safe_file_name_alloc(file_name);
    if (!symlink || symlink[0] == '/') {
	wy_log(LOG_ERR,
	       _("refusing to remove symlink: suspicious symlink source %s"),
	       file_name);
	free(symlink);
	return -1;
    }

    pkg_dir_fd = triplet_open_dest_dir(trp);
    if (pkg_dir_fd != -1) {
	rc = rmsymlink_triplet_file(pkg_dir_fd, trp, symlink);
	if (rc == 0 && archive_signatures
	    && (signame = make_signame(symlink))) {
	    rc = rmsymlink_triplet_file(pkg_dir_fd, trp, signame);
	    free(signame);
	}
	close(pkg_dir_fd);
    } else
	rc = -1;

    free(symlink);
    return rc;
}

int
dir_test_url(wy_url_t url, grecs_locus_t *locus)
{
    int rc;
    const char *dest_dir;

    dest_dir = wy_url_path(url);
    if (!dest_dir) {
	grecs_error(locus, 0, _("cannot extract directory part from URL"));
	return -1;
    }
    if (test_dir(dest_dir, &rc) && rc != ENOENT) {
	if (rc)
	    grecs_error(locus, rc, _("cannot access %s"), dest_dir);
	else
	    grecs_error(locus, 0, _("%s is not a directory"), dest_dir);
	return 1;
    }
    return 0;
}
