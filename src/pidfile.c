/* wydawca - automatic release submission daemon
   Copyright (C) 2007, 2009-2013, 2017, 2019-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include "wydawca.h"

char *pidfile = LOCALSTATEDIR "/run/wydawca.pid";
int force_startup;

void
pidfile_remove(void)
{
    unlink(pidfile);
}

void
pidfile_check(void)
{
    FILE *fp = fopen(pidfile, "r+");

    if (fp) {
	unsigned long pid = -1;
	if (fscanf(fp, "%lu\n", &pid) != 1) {
	    wy_log(LOG_ERR, _("malformed pidfile %s"), pidfile);
	    if (!force_startup)
		exit(EX_UNAVAILABLE);
	} else {
	    if (kill(pid, 0)) {
		if (errno != ESRCH) {
		    wy_log(LOG_ERR,
			   _("cannot verify if PID %lu "
			     "is running: %s"), pid, strerror(errno));
		    if (!force_startup)
			exit(EX_UNAVAILABLE);
		}
	    } else if (!force_startup) {
		wy_log(LOG_ERR,
		       _("another wydawca instance may "
			 "be running (PID %lu)"), pid);
		exit(EX_UNAVAILABLE);
	    }
	}
	wy_log(LOG_NOTICE, _("replacing pidfile %s (PID %lu)"),
	       pidfile, pid);
	fseek(fp, 0, SEEK_SET);
    } else if (errno != ENOENT) {
	wy_log(LOG_ERR, _("cannot open pidfile %s: %s"), pidfile,
	       strerror(errno));
	exit(EX_UNAVAILABLE);
    } else {
	fp = fopen(pidfile, "w");
	if (!fp) {
	    wy_log(LOG_ERR,
		   _("cannot open pidfile %s for writing: %s"),
		   pidfile, strerror(errno));
	    exit(EX_UNAVAILABLE);
	}
    }
    fprintf(fp, "%lu\n", (unsigned long) getpid());
    fclose(fp);
}

void
pidfile_update(void)
{
    FILE *fp = fopen(pidfile, "w");
    if (!fp) {
	wy_log(LOG_ERR,
	       _("cannot open pidfile %s for writing: %s"),
	       pidfile, strerror(errno));
	exit(EX_UNAVAILABLE);
    }
    fprintf(fp, "%lu\n", (unsigned long) getpid());
    fclose(fp);
}
