/* wydawca - automatic release submission daemon
   Copyright (C) 2007, 2009-2013, 2017, 2019-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include "wydawca.h"

struct grecs_sockaddr listen_sockaddr;
size_t max_connections = 16;
time_t idle_timeout = 5;

static int
open_listener()
{
    int fd;

    if (listen_sockaddr.sa == NULL)
	return -1;

    fd = socket(listen_sockaddr.sa->sa_family, SOCK_STREAM, 0);
    if (fd == -1) {
	wy_log(LOG_CRIT, _("cannot create socket: %s"), strerror(errno));
	exit(EX_OSERR);
    }
    if (listen_sockaddr.sa->sa_family == AF_UNIX) {
	struct stat st;
	struct sockaddr_un *s_un =
	    (struct sockaddr_un *) listen_sockaddr.sa;
	if (stat(s_un->sun_path, &st)) {
	    if (errno != ENOENT) {
		wy_log(LOG_CRIT,
		       _("%s: cannot stat socket: %s"),
		       s_un->sun_path, strerror(errno));
		exit(errno == EACCES ? EX_NOPERM : EX_OSERR);
	    }
	} else {
	    /* FIXME: Check permissions? */
	    if (!S_ISSOCK(st.st_mode)) {
		wy_log(LOG_CRIT, _("%s: not a socket"), s_un->sun_path);
		exit(EX_OSFILE);
	    }
	    unlink(s_un->sun_path);
	}
	/* FIXME: Setup umask */
    } else {
	int yes = 1;
	setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (void *) &yes,
		   sizeof(yes));
    }

    if (bind(fd, listen_sockaddr.sa, listen_sockaddr.len) < 0) {
	wy_log(LOG_CRIT, _("cannot bind to local address: %s"),
	       strerror(errno));
	close(fd);
	exit(EX_OSERR);
    }
    if (listen(fd, 8) == -1) {
	wy_log(LOG_CRIT, "listen: %s", strerror(errno));
	close(fd);
	exit(EX_OSERR);
    }

    return fd;
}

void
trim_crlf(char *s)
{
    size_t len = strlen(s);
    if (len > 0 && s[len - 1] == '\n') {
	s[--len] = 0;
	if (len > 0 && s[len - 1] == '\r')
	    s[--len] = 0;
    }
}

struct wydawca_connection {
    struct timespec ts;
    pthread_t tid;
    FILE *fp;
    TAILQ_ENTRY(wydawca_connection) link;
};

typedef TAILQ_HEAD(,wydawca_connection) WYDAWCA_CONNECTION_QUEUE;

static inline void
wydawca_connection_enqueue(WYDAWCA_CONNECTION_QUEUE *q,
			   struct wydawca_connection *conn)
{
    TAILQ_INSERT_TAIL(q, conn, link);
}

static inline void
wydawca_connection_dequeue(WYDAWCA_CONNECTION_QUEUE *q,
			   struct wydawca_connection *conn)
{
    TAILQ_REMOVE(q, conn, link);
}

static struct wydawca_connection *conn_table;
static WYDAWCA_CONNECTION_QUEUE conn_avail = TAILQ_HEAD_INITIALIZER(conn_avail),
				conn_idle = TAILQ_HEAD_INITIALIZER(conn_idle);
static pthread_mutex_t conn_mutex = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t conn_cond = PTHREAD_COND_INITIALIZER;

static void
conn_table_init(void)
{
    size_t i;

    conn_table = grecs_calloc(max_connections, sizeof(conn_table[0]));
    for (i = 1; i < max_connections; i++) {
	wydawca_connection_enqueue(&conn_avail, &conn_table[i]);
    }
}

static void
connection_start(int fd)
{
    struct wydawca_connection *conn;

    pthread_mutex_lock(&conn_mutex);
    if ((conn = TAILQ_FIRST(&conn_avail)) != NULL) {
	wydawca_connection_dequeue(&conn_avail, conn);
	clock_gettime(CLOCK_REALTIME, &conn->ts);
	conn->ts.tv_sec += idle_timeout;
	conn->fp = fdopen(fd, "w+");
	pthread_create(&conn->tid, NULL, wy_thr_tcpmux, conn);
	wydawca_connection_enqueue(&conn_idle, conn);
	pthread_cond_broadcast(&conn_cond);
    } else {
	wy_log(LOG_ERR, "connection table is full");
	close(fd);
    }
    pthread_mutex_unlock(&conn_mutex);
}

static void
connection_unidle(struct wydawca_connection *conn)
{
    pthread_mutex_lock(&conn_mutex);
    fclose(conn->fp);
    conn->fp = NULL;
    wydawca_connection_dequeue(&conn_idle, conn);
    pthread_cond_broadcast(&conn_cond);
    pthread_mutex_unlock(&conn_mutex);
}

static void
connection_stop(struct wydawca_connection *conn)
{
    pthread_mutex_lock(&conn_mutex);
    if (conn->fp) {
	fclose(conn->fp);
	conn->fp = NULL;
	wydawca_connection_dequeue(&conn_idle, conn);
	pthread_cond_broadcast(&conn_cond);
    }
    wydawca_connection_enqueue(&conn_avail, conn);
    pthread_mutex_unlock(&conn_mutex);
}

static inline int
timespec_cmp(struct timespec const *a, struct timespec const *b)
{
    if (a->tv_sec > b->tv_sec)
	return 1;
    if (a->tv_sec < b->tv_sec)
	return -1;
    if (a->tv_nsec > b->tv_nsec)
	return 1;
    if (a->tv_nsec < b->tv_nsec)
	return -1;
    return 0;
}

static void
handle_connection(struct wydawca_connection *conn)
{
    char *buf = NULL;
    size_t buflen = 0;
    struct spool *spool;

    if (grecs_getline(&buf, &buflen, conn->fp) <= 0)
	return;
    trim_crlf(buf);
    wy_debug(1, ("recv: %s", buf));
    spool = wydawca_find_spool(buf);
    if (!spool) {
	if (all_spool_aliases && grecs_list_locate(all_spool_aliases, buf))
	    fprintf(conn->fp, "+ OK, all spools\r\n");
	else {
	    fprintf(conn->fp, "- Unknown service name\r\n");
	    free(buf);
	    return;
	}
    } else if (spool->url)
	fprintf(conn->fp, "+ OK, URL %s\r\n", spool->url);
    else
	fprintf(conn->fp, "+ OK, spool %s\r\n", spool->tag);

    if (grecs_getline(&buf, &buflen, conn->fp) < 0) {
	wy_log(LOG_ERR, "protocol error");
	free(buf);
	return;
    }
    connection_unidle(conn);

    trim_crlf(buf);
    wy_debug(1, ("recv: %s", buf));

    /* FIXME: User name not used any more */

    if (spool)
	scan_spool(spool);
    else
	scan_all_spools();
    free(buf);
}

void *
wy_thr_tcpmux(void *ptr)
{
    struct wydawca_connection *conn = ptr;
    wy_set_cur_thread_name("WY_tcpmux");
    pthread_cleanup_push((void (*)(void*))connection_stop, conn);
    setlinebuf(conn->fp);
    handle_connection(conn);
    pthread_cleanup_pop(1);
    return NULL;
}

void *
wy_thr_connection_watcher(void *ptr)
{
    wy_set_cur_thread_name("WY_connwatch");
    pthread_mutex_lock(&conn_mutex);
    while (1) {
	struct wydawca_connection *conn;

	if ((conn = TAILQ_FIRST(&conn_idle)) != NULL) {
	    struct timespec ts;

	    pthread_cond_timedwait(&conn_cond, &conn_mutex, &conn->ts);
	    if ((conn = TAILQ_FIRST(&conn_idle)) != NULL) {
		clock_gettime(CLOCK_REALTIME, &ts);
		if (timespec_cmp(&ts, &conn->ts) >= 0) {
		    void *ret;
		    pthread_t tid = conn->tid;
		    pthread_cancel(tid);
		    pthread_mutex_unlock(&conn_mutex);
		    pthread_join(tid, &ret);
		    pthread_mutex_lock(&conn_mutex);
		}
	    }
	} else
	    pthread_cond_wait(&conn_cond, &conn_mutex);
    }
}

static inline int
notify_parent(void)
{
    char *p = getenv("WYDAWCA_NOTIFY_PARENT");
    return (p && strcmp(p, "1") == 0);
}

void *
wy_thr_listen(void *ptr)
{
    int ctlfd = open_listener();
    int wfd = watcher_init();
    int maxfd = 0;

    wy_set_cur_thread_name("WY_listener");

    if (ctlfd != -1) {
	pthread_t tid;
	pthread_create(&tid, NULL, wy_thr_connection_watcher, NULL);
	maxfd = ctlfd;
    }

    if (wfd != -1 && wfd > maxfd)
	maxfd = wfd;

    if (maxfd == 0) {
	wy_log(LOG_CRIT,
	       _("listener address is not configured and inotify "
		 "is not available"));
	exit(EX_CONFIG);
    }

    conn_table_init();
    if (notify_parent())
	kill(getppid(), SIGUSR1);

    while (1) {
	int rc;
	fd_set rset;

	FD_ZERO(&rset);
	if (ctlfd != -1)
	    FD_SET(ctlfd, &rset);
	if (wfd != -1)
	    FD_SET(wfd, &rset);

	rc = select(maxfd + 1, &rset, NULL, NULL, NULL);
	if (rc == 0)
	    continue;
	else if (rc < 0) {
	    if (errno == EINTR)
		continue;
	    wy_log(LOG_ERR, "select: %s", strerror(errno));
	    break;
	}

	if (wfd != -1 && FD_ISSET(wfd, &rset)) {
	    watcher_run(wfd);
	}

	if (ctlfd != -1 && FD_ISSET(ctlfd, &rset)) {
	    int fd;
	    union {
		struct sockaddr sa;
		struct sockaddr_in s_in;
		struct sockaddr_un s_un;
	    } addr;
	    socklen_t len;

	    len = sizeof(addr);
	    fd = accept(ctlfd, (struct sockaddr *) &addr, &len);
	    if (fd == -1)
		continue;
	    /* FIXME: Use Mailutils ACLs? */
#ifdef WITH_LIBWRAP
	    if (!tcpwrap_access(fd)) {
		close(fd);
		continue;
	    }
#endif
	    connection_start(fd);
	}
    }
    return NULL;
}
