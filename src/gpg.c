/* wydawca - automatic release submission daemon
   Copyright (C) 2007, 2010-2013, 2017, 2019-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

/* GPG interface functions */

#include "wydawca.h"

char *temp_homedir;

struct cwd {
    struct cwd *prev;
    char *name;
    int fd;
    DIR *dir;
};

static int
cwd_push(struct cwd **tos, char const *name)
{
    struct cwd *ncwd;
    int fd;
    DIR *dir;

    fd = openat(*tos ? (*tos)->fd : AT_FDCWD, name,
		O_RDONLY | O_NONBLOCK | O_DIRECTORY);
    if (fd == -1) {
	return -1;
    }
    dir = fdopendir(fd);
    if (!dir) {
	close(fd);
	return -1;
    }
    ncwd = grecs_malloc(sizeof(*ncwd));
    ncwd->name = grecs_strdup(name);
    ncwd->prev = *tos;
    ncwd->fd = fd;
    ncwd->dir = dir;
    *tos = ncwd;
    return 0;
}

static void
cwd_pop(struct cwd **tos)
{
    if (*tos) {
	struct cwd *cwd = *tos;
	free(cwd->name);
	closedir(cwd->dir);
	*tos = cwd->prev;
	free(cwd);
    }
}

/* Recursively remove the contents of the directory NAME and the directory
   itself.  Do not change CWD. */
int
wy_rmdir_r(const char *name)
{
    struct dirent *ent;
    struct cwd *cwd = NULL;
    int err = 0;

    if (cwd_push(&cwd, name)) {
	if (errno == ENOENT)
	    return 0;
	wy_log(LOG_ERR, _("cannot save current directory: %s"),
	       strerror(errno));
	return 1;
    }

    while (cwd) {
	while ((ent = readdir(cwd->dir)) != NULL) {
	    struct stat st;

	    if (strcmp(ent->d_name, ".") == 0
		|| strcmp(ent->d_name, "..") == 0)
		continue;

	    if (fstatat(cwd->fd, ent->d_name, &st, 0) && errno != ENOENT) {
		wy_log(LOG_ERR, _("cannot stat file `%s': %s"),
		       name, strerror(errno));
		err = 1;
	    } else if (S_ISDIR(st.st_mode)) {
		if (cwd_push(&cwd, ent->d_name))
		    continue;
	    } else if (unlinkat(cwd->fd, ent->d_name, 0) != 0
		       && errno != ENOENT) {
		err = 1;
		wy_log(LOG_ERR, _("cannot unlink %s: %s"),
		       ent->d_name, strerror(errno));
	    }
	}

	if (unlinkat(cwd->prev ? cwd->prev->fd : AT_FDCWD, cwd->name,
		     AT_REMOVEDIR) && errno != ENOENT) {
	    wy_log(LOG_ERR, _("cannot unlink %s: %s"),
		   cwd->name, strerror(errno));
	    err = 1;
	}
	cwd_pop(&cwd);
    }
    return err;
}

char *
wy_tempdir(void)
{
    char *s = grecs_strdup("/tmp/wydawca-XXXXXX");
    if (!mkdtemp(s)) {
	wy_log(LOG_CRIT,
	       _("cannot create temporary directory (%s): %s"),
	       s, strerror(errno));
	free(s);
	s = NULL;
    }
    return s;
}

/* Remove temporary GPG home directory */
static void
remove_homedir(void)
{
    wy_debug(2, (_("removing GNUPG home directory: %s"), temp_homedir));
    if (wy_rmdir_r(temp_homedir)) {
	wy_log(LOG_CRIT, _("failed to remove GPG directory %s"),
	       temp_homedir);
    }
}

/* Create a temporary GPG home directory */
static int
create_gpg_homedir(void)
{
    if (temp_homedir)
	return 0;

    temp_homedir = wy_tempdir();
    if (!temp_homedir)
	return 1;

    atexit(remove_homedir);
    wy_debug(2, (_("GNUPG home directory: %s"), temp_homedir));
    setenv("GNUPGHOME", temp_homedir, 1);
    return 0;
}

static gpgme_ctx_t
triplet_gpgme_ctx(struct wy_triplet *trp)
{
    if (!trp->gpgme_ctx) {
	gpgme_error_t ec;
	struct wy_user *uptr;

	if (create_gpg_homedir())
	    return NULL;
	ec = gpgme_new(&trp->gpgme_ctx);
	if (ec) {
	    wy_log(LOG_ERR, _("failed to initialize gpgme context: %s"),
		   gpgme_strerror(ec));
	    return NULL;
	}
	gpgme_set_offline (trp->gpgme_ctx, 1);

	for (uptr = wy_triplet_get_uploaders(trp); uptr; uptr = uptr->next) {
	    gpgme_import_result_t res;
	    gpgme_import_status_t pstat;
	    gpgme_data_t key_data;

	    ec = gpgme_data_new_from_mem(&key_data,
					 uptr->gpg_key,
					 strlen(uptr->gpg_key), 0);
	    if (ec) {
		wy_log(LOG_ERR, "gpgme_data_new_from_mem: %s",
		       gpgme_strerror(ec));
		continue;
	    }
	    ec = gpgme_op_import(trp->gpgme_ctx, key_data);
	    if (ec) {
		wy_log(LOG_ERR, "gpgme_op_import: %s",
		       gpgme_strerror(ec));
		continue;
	    }

	    res = gpgme_op_import_result(trp->gpgme_ctx);
	    pstat = res->imports;
	    if (pstat) {
		uptr->fpr = grecs_strdup(pstat->fpr);
		wy_debug(3, (_("imported key: user = %s, fingerprint = %s"),
			     uptr->name, uptr->fpr));
	    } else
		wy_log(LOG_NOTICE, _("%s: no key imported"), uptr->name);
	    gpgme_data_release(key_data);
	}
    }
    return trp->gpgme_ctx;
}

void
triplet_gpgme_ctx_release(struct wy_triplet *trp)
{
    if (trp->gpgme_ctx) {
	gpgme_release(trp->gpgme_ctx);
	trp->gpgme_ctx = NULL;
    }
}

static int
checksig(gpgme_ctx_t ctx, gpgme_signature_t sig, struct wy_triplet *trp)
{
    switch (gpg_err_code(sig->status)) {
    case GPG_ERR_NO_ERROR:{
	gpgme_err_code_t err;
	const char *uid;
	const char *fpr;
	gpgme_key_t key;

	err = gpgme_get_key(ctx, sig->fpr, &key, 0);

	if (err != GPG_ERR_NO_ERROR) {
	    wy_log(LOG_ERR, _("can't retrieve key for signature %s: %s"),
		   sig->fpr, gpgme_strerror(err));
	    return 0;
	}
	uid = key->uids->uid;
	fpr = key->fpr;

	wy_debug(1, (_("Good signature from %s"), uid));
	trp->uploader = uploader_find_fpr(trp->uploader_list, fpr);
	if (!trp->uploader) {
	    wy_log(LOG_ERR,
		   _("good signature from %s, "
		     "but the uploader info for %s not found"),
		   uid, sig->fpr);
	}
	break;
    }

    case GPG_ERR_BAD_SIGNATURE:
	wydawca_stat_incr(WY_STAT_BAD_SIGNATURE);
	wy_log(LOG_ERR, _("BAD signature from %s"), sig->fpr);
	return 0;

    case GPG_ERR_NO_PUBKEY:
	wydawca_stat_incr(WY_STAT_ACCESS_VIOLATIONS);
	wy_log(LOG_ERR, _("No public key: %s"), sig->fpr);
	return 0;

    case GPG_ERR_NO_DATA:
	wydawca_stat_incr(WY_STAT_BAD_TRIPLETS);
	wy_log(LOG_ERR, _("No signature"));
	return 0;

    case GPG_ERR_SIG_EXPIRED:
	wydawca_stat_incr(WY_STAT_BAD_SIGNATURE);
	wy_log(LOG_ERR, _("Expired signature from %s"), sig->fpr);
	return 0;

    case GPG_ERR_KEY_EXPIRED:
	wydawca_stat_incr(WY_STAT_BAD_SIGNATURE);
	wy_log(LOG_ERR, _("Key expired (%s)"), sig->fpr);
	return 0;

    default:
	wy_log(LOG_ERR, _("Unknown signature error"));
	return 0;
    }
    return -1;
}

static int
gpg_verify_signature(gpgme_ctx_t ctx, gpgme_signature_t sig,
		     struct wy_triplet *trp)
{
    int res = 0;
    for (; sig; sig = sig->next) {
	res = checksig(ctx, sig, trp);
	if (res == 0)
	    break;
    }
    return res;
}

static int
wy_gpgme_data_new_from_triplet(gpgme_data_t *dh, struct wy_triplet *trp,
			       enum file_type t)
{
    int fd;
    gpgme_error_t ec;

    fd = openat(trp->spool->source_fd, trp->file[t].name, O_RDONLY);
    if (fd == -1) {
	wy_log(LOG_ERR, _("cannot open file %s/%s: %s"),
	       trp->spool->source_dir, trp->file[file_directive].name,
	       strerror(errno));
	return -1;
    }

    ec = gpgme_data_new_from_fd(dh, fd);
    if (ec) {
	close(fd);
	wy_log(LOG_ERR, _("%s: GPGME error: %s"),
	       "gpgme_data_new_from_fd", gpgme_strerror(ec));
	return -1;
    }
    return fd;
}

/* Verify the directive file from TRP using public key PUBKEY */
int
verify_directive_signature(struct wy_triplet *trp)
{
    gpgme_ctx_t ctx;
    gpgme_data_t directive_data, plain = NULL;
    gpgme_error_t ec;
    int rc;
    int fd;

    ctx = triplet_gpgme_ctx(trp);
    if (!ctx)
	return -1;

    fd = wy_gpgme_data_new_from_triplet(&directive_data, trp, file_directive);
    if (fd == -1) {
	return 1;
    }

    gpgme_data_new(&plain);
    ec = gpgme_op_verify(ctx, directive_data, NULL, plain);
    close(fd);
    if (ec == GPG_ERR_NO_ERROR) {
	gpgme_verify_result_t result;

	result = gpgme_op_verify_result(ctx);
	if (!gpg_verify_signature(ctx, result->signatures, trp)) {
	    wydawca_stat_incr(WY_STAT_BAD_SIGNATURE);
	    notify(&trp->spool->notification_queue, trp,
		   trp->uploader ? wy_ev_bad_directive_signature : wy_ev_bad_ownership);
	    rc = 1;
	} else
	    rc = 0;
    } else {
	rc = 1;
	wydawca_stat_incr(WY_STAT_BAD_SIGNATURE);
	wy_log(LOG_ERR, _("%s: directive verification failed: %s"),
	       trp->name, gpgme_strerror(ec));
    }

    gpgme_data_release(plain);
    gpgme_data_release(directive_data);

    return rc;
}

/* Verify the detached signature of TRP. */
int
verify_detached_signature(struct wy_triplet *trp)
{
    int sig_fd, dist_fd;
    gpgme_data_t sig_data, dist_data;
    gpgme_ctx_t ctx;
    gpgme_error_t ec;
    int rc;

    ctx = triplet_gpgme_ctx(trp);
    if (!ctx)
	return -1;

    sig_fd = wy_gpgme_data_new_from_triplet(&sig_data, trp, file_signature);
    if (sig_fd == -1)
	return 1;
    dist_fd = wy_gpgme_data_new_from_triplet(&dist_data, trp, file_dist);
    if (dist_fd == -1) {
	close(sig_fd);
	return 1;
    }

    ec = gpgme_op_verify(ctx, sig_data, dist_data, NULL);
    if (ec == GPG_ERR_NO_ERROR) {
	wy_debug(1, (_("good detached signature for %s"), trp->name));
	rc = 0;
    } else {
	wydawca_stat_incr(WY_STAT_BAD_SIGNATURE);
	wy_log(LOG_ERR, _("BAD detached signature for %s"), trp->name);
	notify(&trp->spool->notification_queue, trp,
	       wy_ev_bad_detached_signature);
	rc = 1;
    }

    gpgme_data_release(sig_data);
    gpgme_data_release(dist_data);

    close(sig_fd);
    close(dist_fd);

    return rc;
}
