/*
  This file is ported from libmicrohttpd
  Copyright (C) 2016 Karlson2k (Evgeny Grin)

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library.  If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <pthread.h>
#ifdef HAVE_PTHREAD_NP_H
# include <pthread_np.h>
#endif /* HAVE_PTHREAD_NP_H */

#if defined(HAVE_PTHREAD_ATTR_SETNAME_NP_NETBSD) \
    || defined(HAVE_PTHREAD_ATTR_SETNAME_NP_IBMI)
# define MHD_USE_THREAD_ATTR_SETNAME 1
#endif /* HAVE_PTHREAD_ATTR_SETNAME_NP_NETBSD || HAVE_PTHREAD_ATTR_SETNAME_NP_IBMI */

#if defined(HAVE_PTHREAD_SETNAME_NP_GNU) \
    || defined(HAVE_PTHREAD_SET_NAME_NP_FREEBSD) \
    || defined(HAVE_PTHREAD_SETNAME_NP_NETBSD)

static void
set_thread_name(pthread_t thread_id, const char *thread_name)
{
    if (thread_name == NULL)
	return;

# if defined(HAVE_PTHREAD_SETNAME_NP_GNU)
    pthread_setname_np(thread_id, thread_name);
# elif defined(HAVE_PTHREAD_SET_NAME_NP_FREEBSD)
    /* FreeBSD and OpenBSD use different name and void return type */
    pthread_set_name_np (thread_id, thread_name);
# elif defined(HAVE_PTHREAD_SETNAME_NP_NETBSD)
    /* NetBSD use 3 arguments: second argument is string in printf-like format,
     *                         third argument is single argument for printf;
     * OSF1 use 3 arguments too, but last one always must be zero (NULL).
     * MHD doesn't use '%' in thread names, so both form are used in same way.
     */
    pthread_setname_np (thread_id, thread_name, 0);
# endif /* HAVE_PTHREAD_SETNAME_NP_NETBSD */
}

# define WY_SET_CUR_THREAD_NAME(n) set_thread_name(pthread_self(),(n))
#elif defined(HAVE_PTHREAD_SETNAME_NP_DARWIN)
# define WY_SET_CUR_THREAD_NAME(n) pthread_setname_np((n))
#else
# define WY_SET_CUR_THREAD_NAME(n)
#endif

void
wy_set_cur_thread_name(char const *name)
{
    WY_SET_CUR_THREAD_NAME(name);
}
