/* wydawca - automatic release submission daemon
   Copyright (C) 2009-2013, 2017, 2019-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include "wydawca.h"

struct wy_url {
    char *printable;
    char *scheme;
    char *path;
    int local:1;
};

wy_url_t
wy_url_create(const char *str)
{
    wy_url_t url;

    url = grecs_zalloc(sizeof(url[0]));
    url->printable = grecs_strdup(str);
    if (*str == '/') {
	url->scheme = grecs_strdup("file");
	url->path = grecs_strdup(str);
	url->local = 1;
    } else {
	size_t len = strcspn(str, ":");
	if (!str[len]) {
	    wy_url_free(url);
	    errno = EINVAL;
	    return NULL;
	}
	url->scheme = grecs_malloc(len + 1);
	memcpy(url->scheme, str, len);
	url->scheme[len] = 0;

	if (str[len + 1] && strncmp(str + len + 1, "//", 2) == 0)
	    len += 2;
	if (str[len + 1])
	    url->path = grecs_strdup(str + len + 1);
	else
	    url->path = NULL;
	url->local = 0;
    }
    return url;
}

void
wy_url_free(wy_url_t url)
{
    free(url->printable);
    free(url->scheme);
    free(url->path);
    free(url);
}

const char *
wy_url_path(wy_url_t url)
{
    return url->path;
}

const char *
wy_url_scheme(wy_url_t url)
{
    return url->scheme;
}

const char *
wy_url_printable(wy_url_t url)
{
    return url->printable;
}

int
wy_url_is_local(wy_url_t url)
{
    return url && url->local;
}

struct virt_tab_reg {
    char *scheme;
    struct virt_tab vtab;
};

static struct virt_tab_reg reg[] = {
    { "file",
      { dir_test_url, dir_move_file, dir_archive_file,
	dir_symlink_file, dir_rmsymlink_file } },
    { "dir",
      { dir_test_url, dir_move_file, dir_archive_file,
	dir_symlink_file, dir_rmsymlink_file } },
    { "null",
      { NULL, null_move_file, null_archive_file, null_symlink_file,
	null_rmsymlink_file } },
    { NULL }
};

int
url_to_vtab(wy_url_t url, struct virt_tab *vtab)
{
    const char *scheme = wy_url_scheme(url);
    int i;

    if (!scheme)
	return 1;
    for (i = 0; reg[i].scheme; i++)
	if (strcmp(reg[i].scheme, scheme) == 0) {
	    *vtab = reg[i].vtab;
	    return 0;
	}
    return 1;
}

int
move_file(struct wy_triplet *trp, enum file_type file_id)
{
    int rc = trp->spool->vtab.move_file(trp, file_id);
    triplet_report_add(trp, "Move %s to %s: %s", trp->file[file_id].name,
		       trp->relative_dir, rc == 0 ? "OK" : "FAILED");
    return rc;
}

int
archive_file(struct wy_triplet *trp, const char *file_name)
{
    int rc = trp->spool->vtab.archive_file(trp, file_name);
    triplet_report_add(trp, "Archive and remove %s/%s: %s",
		       trp->relative_dir, file_name,
		       rc == 0 ? "OK" : "FAILED");
    return rc;
}

int
symlink_file(struct wy_triplet *trp,
	     const char *wanted_src, const char *wanted_dst)
{
    int rc = trp->spool->vtab.symlink_file(trp, wanted_src, wanted_dst);
    triplet_report_add(trp, "Symlink %s to %s in %s/: %s",
		       wanted_src, wanted_dst,
		       trp->relative_dir, rc == 0 ? "OK" : "FAILED");
    return rc;
}

int
rmsymlink_file(struct wy_triplet *trp, const char *file_name)
{
    int rc = trp->spool->vtab.rmsymlink_file(trp, file_name);
    triplet_report_add(trp, "Remove symlink %s/%s: %s",
		       trp->relative_dir, file_name,
		       rc == 0 ? "OK" : "FAILED");
    return rc;
}
