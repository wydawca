/* wydawca - automatic release submission daemon
   Copyright (C) 2009-2013, 2017, 2019-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include "wydawca.h"

int
null_move_file(struct wy_triplet *trp, enum file_type file_id)
{
    const struct spool *spool = trp->spool;
    const char *file_name = trp->file[file_id].name;
    wy_debug(1, (_("spool %s: installing file `%s/%s'"),
		 spool->tag, trp->relative_dir, file_name));
    wydawca_stat_incr(WY_STAT_UPLOADS);
    if (!wy_dry_run && unlink(file_name)) {
	wy_log(LOG_ERR, _("cannot unlink %s: %s"),
	       file_name, strerror(errno));
	return 1;
    }
    return 0;
}

int
null_archive_file(struct wy_triplet *trp, const char *file_name)
{
    wy_debug(1, (_("spool %s: archiving `%s'"),
		 trp->spool->tag, file_name));
    wydawca_stat_incr(WY_STAT_ARCHIVES);
    return 0;
}

int
null_symlink_file(struct wy_triplet *trp,
		  const char *wanted_src, const char *wanted_dst)
{
    wy_debug(1, (_("spool %s: symlinking `%s' to `%s'"),
		 trp->spool->tag, wanted_src, wanted_dst));
    wydawca_stat_incr(WY_STAT_SYMLINKS);
    return 0;
}

int
null_rmsymlink_file(struct wy_triplet *trp, const char *file_name)
{
    wy_debug(1, (_("spool %s: removing symlink `%s/%s'"),
		 trp->spool->tag, trp->relative_dir, file_name));
    wydawca_stat_incr(WY_STAT_RMSYMLINKS);
    return 0;
}
