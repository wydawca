/* wydawca - automatic release submission daemon
   Copyright (C) 2007-2013, 2017, 2019-2023 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program. If not, see <http://www.gnu.org/licenses/>. */

#define wy_s_cat2(a,b) a ## b
#define wy_s_cat3(a,b,c) a ## b ## c

#define WY_EXPORT(module,name) wy_s_cat2(wy_,name)
#define wy_notify WY_EXPORT(WY_MODULE,notify)
#define wy_help WY_EXPORT(WY_MODULE,help)
#define wy_open WY_EXPORT(WY_MODULE,open)
#define wy_config WY_EXPORT(WY_MODULE,config)
#define wy_flush WY_EXPORT(WY_MODULE,flush)

/* Global variables */

extern int wy_dry_run;	/* Dry run indicator */
extern int wy_debug_level;	/* Debugging level */
extern char *wy_gpg_homedir;
extern const char *wy_version;
extern int wy_log_to_stderr;	/* Log to stderr instead of the syslog */
extern int wy_log_facility;	/* Syslog facility to use if !wy_log_to_stderr */
extern char *wy_syslog_tag;	/* Syslog tag */

/* Logging and debugging */
#ifndef WY_PRINTFLIKE
# define WY_PRINTFLIKE(fmt,narg) __attribute__ ((__format__ (__printf__, fmt, narg)))
#endif

void wy_log(int prio, char const *fmt, ...) WY_PRINTFLIKE(2, 3);
void wy_vlog(int prio, char const *fmt, va_list ap);
void wy_dbg(char const *fmt, ...) WY_PRINTFLIKE(1, 2);
#define wy_debug(l,c) do {				\
		if (wy_debug_level >= (l))		\
			wy_dbg c;			\
	} while(0)

/* Wydawca events */
enum wy_event {
	wy_ev_success,
	wy_ev_bad_ownership,
	wy_ev_bad_directive_signature,
	wy_ev_bad_detached_signature,
	wy_ev_check_fail,
	wy_ev_stat,
	WY_N_EVENT
};

const char *wy_event_str(enum wy_event evt);

enum wydawca_stat {
	WY_STAT_ERRORS,
	WY_STAT_WARNINGS,
	WY_STAT_BAD_SIGNATURE,
	WY_STAT_ACCESS_VIOLATIONS,
	WY_STAT_COMPLETE_TRIPLETS,
	WY_STAT_INCOMPLETE_TRIPLETS,
	WY_STAT_BAD_TRIPLETS,
	WY_STAT_EXPIRED_TRIPLETS,
	WY_STAT_TRIPLET_SUCCESS,
	WY_STAT_UPLOADS,
	WY_STAT_ARCHIVES,
	WY_STAT_SYMLINKS,
	WY_STAT_RMSYMLINKS,
	WY_STAT_CHECK_FAIL,
	WY_MAX_STAT
};

#define WY_STAT_MASK(c) (1<<(c))
#define WY_STAT_MASK_NONE 0
#define WY_STAT_MASK_ALL  (WY_STAT_MASK(WY_MAX_STAT) - 1)

struct wy_user {
	struct wy_user *next;
	char *name;
	char *realname;
	char *email;
	char *gpg_key;
	char *fpr;
};

typedef struct wy_triplet wy_triplet_t;

const char *wy_triplet_project(wy_triplet_t *trp);
struct wy_user *wy_triplet_get_uploaders(wy_triplet_t *trp);
struct wy_user *wy_triplet_get_admins(wy_triplet_t *trp);
struct wy_user *wy_triplet_get_uploader(wy_triplet_t *trp);

struct wy_vardef {
	char const *name;
	int (*expand)(char **ret, struct wy_triplet *trp);
};
char *wy_triplet_expand_param(const char *tmpl, struct wy_triplet *trp,
			      struct wy_vardef *xmeta);

char *wy_expand_stats(const char *tmpl);
int wy_stat_mask_p(unsigned long mask);
int wy_expand_copy(char **ret, char const *s);
