/* wydawca - automatic release submission daemon
   Copyright (C) 2007-2013, 2017, 2019-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program. If not, see <http://www.gnu.org/licenses/>. */

int wy_assert_string_arg(grecs_locus_t *locus,
			 enum grecs_callback_command cmd,
			 const grecs_value_t *value);
int wy_cb_statistics(enum grecs_callback_command cmd, grecs_node_t *node,
		     void *varptr, void *cb_data);
