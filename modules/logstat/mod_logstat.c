/* wydawca - automatic release submission daemon
   Copyright (C) 2007-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <grecs.h>
#include <wydawca/wydawca.h>
#include <wydawca/cfg.h>

#define WY_MODULE mod_logstat

static int stat_mask = WY_STAT_MASK_ALL;
static struct grecs_list *log_msg;

static struct grecs_keyword logstat_kw[] = {
    { "statistics", N_("items"),
      N_("Log these statistics items"),
      grecs_type_string, GRECS_DFLT, &stat_mask, 0,
      wy_cb_statistics },
    { "message", N_("text"),
      N_("Log additional message"),
      grecs_type_string, GRECS_LIST|GRECS_AGGR, &log_msg, 0, NULL },
    { NULL }
};


void *
wy_config(grecs_node_t *node)
{
    grecs_tree_reduce(node, logstat_kw, 0);
    if (grecs_tree_process(node->down, logstat_kw))
	return NULL;
    return &stat_mask;
}

static void
logmsg(char *s)
{
    char *p;

    while ((p = strchr(s, '\n'))) {
	*p++ = 0;
	wy_log(LOG_INFO, "%s", s);
	s = p;
    }
    if (*s)
	wy_log(LOG_INFO, "%s", s);
}

static char *default_log_message[] = {
    "errors: ${stat:errors}",
    "warnings: ${stat:warnings}",
    "bad signatures: ${stat:bad_signatures}",
    "access violation attempts: ${stat:access_violations}",
    "complete triplets: ${stat:complete_triplets}",
    "incomplete triplets: ${stat:incomplete_triplets}",
    "bad triplets: ${stat:bad_triplets}",
    "expired triplets: ${stat:expired_triplets}",
    "triplet successes: ${stat:triplet_success}",
    "files uploaded: ${stat:uploads}",
    "files archived: ${stat:archives}",
    "symlinks created: ${stat:symlinks}",
    "symlinks removed: ${stat:rmsymlinks}",
    "check failures: ${stat:check_failures}",
    NULL
};

void
wy_notify(void *data, int ev, wy_triplet_t const *trp)
{
    struct grecs_list_entry *ep;
    char *text;

    if (ev != wy_ev_stat || !wy_stat_mask_p(stat_mask))
	return;

    if (log_msg) {
	for (ep = log_msg->head; ep; ep = ep->next) {
	    text = wy_expand_stats(ep->data);
	    logmsg(text);
	    free(text);
	}
    } else {
	int i;
	for (i = 0; default_log_message[i]; i++) {
	    if (wy_stat_mask_p(WY_STAT_MASK(i))) {
		text = wy_expand_stats(default_log_message[i]);
		logmsg(text);
		free(text);
	    }
	}
    }
}

void
wy_help(void)
{
    static struct grecs_keyword top[] = {
	{ "module-config", NULL,
	  "module configuration",
	  grecs_type_section, GRECS_INAC, NULL, 0, NULL, NULL,
	  logstat_kw },
	{ NULL }
    };
    static char const *docstring = "\n\
mod_logstat sends statistics reports to syslog\n";

    puts(docstring);
    printf("# Usage in notify-event statement:\n");
    printf("notify-event {\n  event statistics;\n  module logstat;");
    grecs_print_statement_array(top, 1, 1, stdout);
    printf("}\n");
}
