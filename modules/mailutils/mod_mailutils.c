/* wydawca - automatic release submission daemon
   Copyright (C) 2007-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <gpgme.h>
#include "grecs.h"
#include <wydawca/wydawca.h>
#include <wydawca/cfg.h>
#include <mailutils/mailutils.h>
#include <wydawca/wordsplit.h>
#include <pthread.h>

#define WY_MODULE mod_mailutils

static char *mailer_url;
static mu_address_t admin_address;
static mu_address_t from_address;
unsigned long mail_admin_mask;
unsigned long owner_notification_flags;

static char *admin_stat_message;
static char *admin_stat_sign_key;

struct message_template {
    char *name;
    char *text;
    /*  int mime; for future use */
};

static struct grecs_symtab *tmpl_table;

/* Register a template. */
void
register_message_template(const char *name, const char *text)
{
    struct message_template key, *tmpl;
    int install = 1;

    key.name = (char *)text;

    if (!tmpl_table) {
	tmpl_table = grecs_symtab_create_default(sizeof(key));
	if (!tmpl_table)
	    grecs_alloc_die();
    }

    tmpl = grecs_symtab_lookup_or_install(tmpl_table, &key, &install);
    if (!tmpl)
	grecs_alloc_die();
    if (!install)
	grecs_warning(NULL, 0, "template %s already registered", text);
}

const char *
resolve_message_template(const char *name)
{
    if (name[0] == '@') {
	if (name[1] == '@')
	    return name + 1;
	else if (!tmpl_table)
	    return NULL;
	else {
	    struct message_template *p, key;

	    key.name = (char *)name + 1;
	    p = grecs_symtab_lookup_or_install(tmpl_table, &key,  NULL);
	    return p ? p->text : NULL;
	}
    }
    return name;
}

static int
cb_define_message(enum grecs_callback_command cmd, grecs_node_t *node,
		  void *varptr, void *cb_data)
{
    const char *ident;
    grecs_locus_t *locus = &node->locus;
    grecs_value_t *value = node->v.value;

    if (cmd != grecs_callback_set_value) {
	grecs_error(locus, 0, _("Unexpected block statement"));
	return 1;
    }
    if (!value || value->type != GRECS_TYPE_ARRAY || value->v.arg.c != 2) {
	grecs_error(locus, 0, _("expected two arguments"));
	return 1;
    }

    if (value->v.arg.v[0]->type != GRECS_TYPE_STRING) {
	grecs_error(&value->v.arg.v[0]->locus, 0,
		    _("first argument not a string"));
	return 1;
    }
    ident = value->v.arg.v[0]->v.string;

    if (value->v.arg.v[1]->type != GRECS_TYPE_STRING) {
	grecs_error(&value->v.arg.v[1]->locus, 0,
		    _("second argument not a string"));
	return 1;
    }

    register_message_template(ident, value->v.arg.v[1]->v.string);
    return 0;
}

static ssize_t
mu_stream_data_read_cb(void *handle, void *buffer, size_t size)
{
    mu_stream_t str = handle;
    size_t nread;
    int rc;

    rc = mu_stream_read(str, buffer, size, &nread);
    if (rc) {
	wy_log(LOG_ERR, "mu_stream_read: %s", mu_strerror(rc));
	errno = EIO;
	return -1;
    }
    return nread;
}

static int
check_sign_result(gpgme_sign_result_t result, gpgme_sig_mode_t type)
{
    gpgme_new_signature_t sign;

    if (result->invalid_signers) {
	wy_log(LOG_ERR, _("GPGME: invalid signer found: %s"),
	       result->invalid_signers->fpr);
	return 1;
    }

    if (!result->signatures) {
	wy_log(LOG_ERR, _("GPGME: no signatures created"));
	return 1;
    }

    for (sign = result->signatures; sign; sign = sign->next) {
	if (sign->type != type) {
	    wy_log(LOG_ERR, _("GPGME: wrong type of signature created"));
	    return 1;
	}
    }
    /* FIXME: fingerprint? */
    return 0;
}

static int
gpg_sign(gpgme_data_t * output, gpgme_data_t input, const char *sign_keys)
{
    gpgme_ctx_t ctx;
    gpgme_error_t err = 0;
    gpgme_key_t key;
    int ret;

    err = gpgme_new(&ctx);
    if (err) {
	wy_log(LOG_ERR, _("GPGME: cannot create context: %s"),
	       gpgme_strerror(err));
	return 1;
    }

    err = gpgme_op_keylist_start(ctx, sign_keys, 0);
    if (!err) {
	while ((err = gpgme_op_keylist_next(ctx, &key)) == 0) {
	    err = gpgme_signers_add(ctx, key);
	    gpgme_key_release(key);
	}
    }

    if (err && gpg_err_code(err) != GPG_ERR_EOF) {
	wy_log(LOG_ERR, _("GPGME: cannot list keys: %s"),
	       gpgme_strerror(err));
	gpgme_release(ctx);
	return 1;
    }

    err = gpgme_data_new(output);
    if (err) {
	wy_log(LOG_ERR, _("%s: GPGME error: %s"),
	       "gpgme_data_new", gpgme_strerror(err));
	gpgme_release(ctx);
	return 1;
    }

    /* FIXME: Passphrase */
    gpgme_set_textmode(ctx, 1);
    gpgme_set_armor(ctx, 1);

    err = gpgme_op_sign(ctx, input, *output, GPGME_SIG_MODE_CLEAR);
    if (err) {
	wy_log(LOG_ERR, _("%s: GPGME error: %s"),
	       "gpgme_op_sign", gpgme_strerror(err));
	ret = 1;
    } else {
	ret = check_sign_result(gpgme_op_sign_result(ctx),
				GPGME_SIG_MODE_CLEAR);
#if 0
	/* FIXME: */
	if (wy_debug_level > 1)
	    gpgme_debug_info(ctx);
#endif
    }

    gpgme_release(ctx);
    return ret;
}

static int
sign_message(mu_message_t *pmsg, const char *key)
{
    mu_message_t msg = *pmsg;
    mu_message_t newmsg;
    mu_body_t body;
    mu_header_t hdr;
    mu_stream_t istr, ostr, bodystr;
    int rc;
    struct gpgme_data_cbs cbs;
    gpgme_data_t input, output;
    gpgme_error_t err;
    char *buf = NULL;
    size_t size = 0;
    size_t nread;

    wy_debug(1, (_("signing message as %s"), key));

    if (wy_gpg_homedir) {
	wy_debug(2, (_("setting GNUPG home directory: %s"),
		     wy_gpg_homedir));
	setenv("GNUPGHOME", wy_gpg_homedir, 1);
    }

    if ((rc = mu_message_get_body(msg, &body))) {
	wy_log(LOG_ERR, "mu_message_get_body: %s", mu_strerror(rc));
	return 1;
    }

    if ((rc = mu_body_get_streamref(body, &bodystr))) {
	wy_log(LOG_ERR, "mu_message_get_stream: %s", mu_strerror(rc));
	return 1;
    }

    memset(&cbs, 0, sizeof(cbs));
    cbs.read = mu_stream_data_read_cb;

    err = gpgme_data_new_from_cbs(&input, &cbs, &bodystr);
    if (err) {
	wy_log(LOG_ERR, "gpgme_data_new_from_cbs: %s",
	       gpgme_strerror(rc));
	return 1;
    }

    rc = gpg_sign(&output, input, key);
    mu_stream_unref(bodystr);
    if (rc)
	return 1;

    if (gpgme_data_seek(output, 0, SEEK_SET) == -1) {
	wy_log(LOG_ERR, "gpgme_data_seek: %s", strerror(errno));
	return 1;
    }

    mu_message_create(&newmsg, NULL);
    mu_message_get_streamref(newmsg, &ostr);

    /* Copy headers */
    mu_message_get_header(msg, &hdr);
    mu_header_get_streamref(hdr, &istr);

    rc = mu_stream_copy(ostr, istr, 0, NULL);
    if (rc)
	wy_log(LOG_ERR, "mu_stream_copy: %s", mu_strerror(rc));
    else {
	while ((nread = gpgme_data_read(output, buf, size)) > 0) {
	    rc = mu_stream_write(ostr, buf, nread, NULL);
	    if (rc) {
		wy_log(LOG_ERR, "mu_stream_write: %s",
		       mu_strerror(rc));
		break;
	    }
	}

	if (rc == 0) {
	    mu_message_destroy(&msg, mu_message_get_owner(msg));
	    *pmsg = newmsg;
	}
    }
    mu_stream_unref(istr);
    mu_stream_unref(ostr);

    if (rc)
	mu_message_destroy(&newmsg, mu_message_get_owner(msg));
    gpgme_data_release(output);
    free(buf);

    return rc;
}

static void
mail_send_message(mu_address_t rcpt, const char *text, const char *signer_key)
{
    int rc;
    mu_mailer_t mailer;
    mu_message_t msg;
    mu_stream_t stream = NULL;
    mu_header_t hdr;
    const char *sval;

    mu_static_memory_stream_create(&stream, text, strlen(text));
    rc = mu_stream_to_message(stream, &msg);
    mu_stream_unref(stream);
    if (rc) {
	wy_log(LOG_CRIT, _("cannot create message: %s"), mu_strerror(rc));
	return;
    }
    mu_message_get_header(msg, &hdr);
    mu_header_append(hdr, "X-Mailer", wy_version);

    if (rcpt) {
	const char *s;

	if (mu_address_sget_printable(rcpt, &s) == 0)
	    mu_header_set_value(hdr, "To", s, 1);

	if (from_address && mu_header_sget_value(hdr, "From", &sval)) {
	    if (mu_address_sget_printable(from_address, &s) == 0)
		mu_header_set_value(hdr, "From", s, 1);
	}
    }

    if (wy_debug_level > 1) {
	mu_debug_level_t level;

	mu_debug_get_category_level(MU_DEBCAT_MAILER, &level);
	level |= MU_DEBUG_LEVEL_MASK(MU_DEBUG_TRACE0)
		 | MU_DEBUG_LEVEL_MASK(MU_DEBUG_PROT);
	if (wy_debug_level > 2)
	    level |= MU_DEBUG_LEVEL_MASK(MU_DEBUG_TRACE7);
	mu_debug_set_category_level(MU_DEBCAT_MAILER, level);
    }

    rc = mu_mailer_create(&mailer, mailer_url);
    if (rc) {
	wy_log(LOG_CRIT, _("cannot create mailer `%s': %s"),
	       mailer_url,
	       mu_strerror(rc));
    } else {
	if ((rc = mu_mailer_open(mailer, 0))) {
	    mu_url_t url = NULL;
	    mu_mailer_get_url(mailer, &url);
	    wy_log(LOG_CRIT, _("opening mailer `%s' failed: %s"),
		   url ? mu_url_to_string(url) : "(unknown URL)",
		   mu_strerror(rc));
	} else {
	    if (signer_key)
		sign_message(&msg, signer_key);

	    if (!wy_dry_run) {
		rc = mu_mailer_send_message(mailer, msg, from_address, rcpt);
		if (rc)
		    wy_log(LOG_CRIT, _("cannot send message: %s"),
			   mu_strerror(rc));
	    }

	    mu_mailer_close(mailer);
	}
	mu_mailer_destroy(&mailer);
    }

    mu_message_destroy(&msg, mu_message_get_owner(msg));
}

static int
cb_mailer(enum grecs_callback_command cmd, grecs_node_t *node,
	  void *varptr, void *cb_data)
{
    int rc;
    grecs_locus_t *locus = &node->locus;
    grecs_value_t *value = node->v.value;
    mu_mailer_t mailer;

    if (wy_assert_string_arg(locus, cmd, value))
	return 1;
    rc = mu_mailer_create(&mailer, value->v.string);
    if (rc)
	grecs_error(&value->locus, 0,
		    _("cannot create mailer `%s': %s"),
		    value->v.string,
		    mu_strerror(rc));
    else {
	mu_mailer_destroy(&mailer);
	mailer_url = grecs_strdup(value->v.string);
    }
    return rc;
}

static int
cb_email_address(enum grecs_callback_command cmd, grecs_node_t *node,
		 void *varptr, void *cb_data)
{
    int rc = 1;
    mu_address_t addr = NULL;
    grecs_locus_t *locus = &node->locus;
    grecs_value_t *value = node->v.value;
    struct grecs_list_entry *ep;

    switch (value->type) {
    case GRECS_TYPE_STRING:
	rc = mu_address_create(&addr, value->v.string);
	if (rc) {
	    grecs_error(&value->locus, 0,
			_("%s: invalid email address: %s"),
			value->v.string, mu_strerror(rc));
	    return rc;
	}
	break;

    case GRECS_TYPE_LIST:
	for (ep = value->v.list->head; ep; ep = ep->next) {
	    const grecs_value_t *vp = ep->data;
	    mu_address_t a;
	    if (wy_assert_string_arg(locus, cmd, vp))
		return 1;

	    rc = mu_address_create(&a, vp->v.string);
	    if (rc == 0)
		rc = mu_address_union(&addr, a);
	    else {
		grecs_error(&value->locus, 0,
			    _("%s: invalid email address: %s"),
			    vp->v.string,
			    mu_strerror(rc));
	    }
	    mu_address_destroy(&a);
	    if (rc)
		break;
	}
	break;

    case GRECS_TYPE_ARRAY:
	grecs_error(locus, 0, _("too many arguments"));
	return 1;
    }

    *(mu_address_t *) varptr = addr;
    return rc;
}

static struct grecs_keyword mail_statistics_kw[] = {
    { "message", N_("text"),
      N_("Message text"),
      grecs_type_string, GRECS_DFLT, &admin_stat_message },
    { "statistics", N_("items"),
      N_("Send mail if one or more of these items are set"),
      grecs_type_string, GRECS_DFLT, &mail_admin_mask, 0,
      wy_cb_statistics },
    { "gpg-sign",
      N_("key"), N_("Sign message with this key"),
      grecs_type_string, GRECS_DFLT, &admin_stat_sign_key },
    { NULL }
};

static struct grecs_keyword mail_kw[] = {
    { "mailer", N_("url"), N_("Set mailer URL"),
      grecs_type_string, GRECS_DFLT, NULL, 0, cb_mailer },
    { "admin-address", N_("email"), N_("Set admin email address"),
      grecs_type_string, GRECS_DFLT, &admin_address, 0, cb_email_address },
    { "from-address", N_("email"), N_("Set sender email address"),
      grecs_type_string, GRECS_DFLT, &from_address, 0, cb_email_address },
    { "define-message", N_("ident: string> <text: string"),
      N_("Define message text"),
      grecs_type_string, GRECS_DFLT, NULL, 0, cb_define_message },
    { "mail-statistics", NULL, N_("Send statistics"),
      grecs_type_section, GRECS_DFLT, NULL, 0, NULL, NULL,
      mail_statistics_kw },

    { NULL }
};

int
wy_open(grecs_node_t *node)
{
    int rc = 0;

    mu_register_all_mailer_formats();
    mu_stdstream_setup(MU_STDSTREAM_RESET_NONE);
    mu_log_tag = wy_syslog_tag;
    mu_log_facility = wy_log_facility;
    mu_stdstream_strerr_setup(wy_log_to_stderr ?
			      MU_STRERR_STDERR : MU_STRERR_SYSLOG);

    if (node) {
	rc = grecs_tree_process(node->down, mail_kw);
	if (rc)
	    return rc;
    }
    return rc;
}

/* Replaces enum notification_target */
enum ntfrcpt {
    notify_read,	    /* Read recipients from the message headers */
    notify_admin,	    /* System administrator */
    notify_owner,	    /* Project admin */
    notify_user		    /* User (uploader) */
};
struct mailevt {
    enum ntfrcpt rcpt;      /* whom to notify */
    const char *msg;        /* message template */
    const char *sign_keys;  /* GPG keys to sign the message with */
};

static struct mu_kwd target_tab[] = {
    { "read",    notify_read },    /* Read recipients from the message
				      headers */
    { "message", notify_read },
    { "admin",   notify_admin },   /* System administrator */
    { "owner",   notify_owner },   /* Project admin */
    { "user",    notify_user },    /* User (uploader) */
    { NULL }
};

static const char *
ntfrcpt_str(enum ntfrcpt tgt)
{
    const char *ret;

    if (mu_kwd_xlat_tok(target_tab, tgt, &ret)) {
	grecs_error(NULL, 0,
		    _("INTERNAL ERROR: "
		      "unknown notification target number: %d"),
		    tgt);
	return NULL;
    }
    return ret;
}

static int
string_to_ntfrcpt(grecs_locus_t *locus, const char *val, enum ntfrcpt *pret)
{
    int res;
    if (mu_kwd_xlat_name(target_tab, val, &res)) {
	grecs_error(locus, 0, _("unknown notification target: %s"), val);
	return 1;
    }
    *pret = res;
    return 0;
}

static int
cb_recipient(enum grecs_callback_command cmd, grecs_node_t *node,
	     void *varptr, void *cb_data)
{
    enum ntfrcpt *tgt = varptr;
    grecs_locus_t *locus = &node->locus;
    grecs_value_t *value = node->v.value;

    if (wy_assert_string_arg(locus, cmd, value))
	return 1;
    string_to_ntfrcpt(&value->locus, value->v.string, tgt);
    return 0;
}

static struct grecs_keyword notify_event_kw[] = {
    { "recipient", N_("who"), N_("Notify this recipient"),
      grecs_type_string, GRECS_DFLT,
      NULL, offsetof(struct mailevt, rcpt),
      cb_recipient },
    { "gpg-sign", N_("key"),
      N_("Sign message with this key"),
      grecs_type_string, GRECS_DFLT,
      NULL, offsetof(struct mailevt, sign_keys) },
    { "message", N_("text-or-id"),
      N_("Text of the notification or identifier of a defined "
	 "message template"),
      grecs_type_string, GRECS_DFLT,
      NULL, offsetof(struct mailevt, msg) },
    { NULL }
};

void *
wy_config(grecs_node_t *node)
{
    int i;
    struct mailevt *evt = grecs_zalloc(sizeof(*evt));

    for (i = 0; notify_event_kw[i].ident; i++)
	notify_event_kw[i].varptr = evt;
    if (grecs_tree_process(node->down, notify_event_kw)) {
	free(evt);
	evt = NULL;
    }
    return evt;
}

static mu_address_t
get_uploader_email(wy_triplet_t *trp, const char **errp)
{
    struct wy_user const *info = wy_triplet_get_uploader(trp);
    mu_address_t addr;
    mu_address_t rcpt = NULL;
    int rc;

    if (!info) {
	*errp = _("user unknown");
	return NULL;
    }
    rc = mu_address_create(&addr, info->email);
    if (rc) {
	*errp = mu_strerror(rc);
	return NULL;
    }

    mu_address_set_personal(addr, 1, info->realname);

    /* This hack makes sure that mu_address_to_string (rcpt) will
       return full email address (with personal part) */
    mu_address_union(&rcpt, addr);
    mu_address_destroy(&addr);

    return rcpt;
}

static mu_address_t
get_owner_address(wy_triplet_t *trp)
{
    struct wy_user *wp;
    mu_address_t rcpt = NULL;

    for (wp = wy_triplet_get_admins(trp); wp; wp = wp->next) {
	mu_address_t addr;
	if (mu_address_create(&addr, wp->email))
	    continue;
	if (wp->realname)
	    mu_address_set_personal(addr, 1, wp->realname);

	mu_address_union(&rcpt, addr);
	mu_address_destroy(&addr);
    }

    return rcpt;
}

static int
expand_email_admin(char **ret, struct wy_triplet *trp)
{
    int rc;
    size_t size = 0;

    rc = mu_address_aget_printable(admin_address, ret);
    if (rc == 0)
	return WRDSE_OK;
    *ret = NULL;
    if (grecs_asprintf(ret, &size, "mu_address_aget_printable: %s",
		       mu_strerror(rc)))
	return WRDSE_NOSPACE;
    return WRDSE_USERERR;
}

static int
expand_email_owner(char **ret, struct wy_triplet *trp)
{
    mu_address_t addr;
    int rc;

    addr = get_owner_address(trp);
    if (!addr) {
	wy_log(LOG_ERR, _("cannot get email of the %s's owner"),
	       wy_triplet_project(trp));
	return WRDSE_UNDEF;
    } else {
	rc = mu_address_aget_printable(addr, ret);
	mu_address_destroy(&addr);
	if (rc) {
	    size_t size = 0;
	    *ret = NULL;
	    if (grecs_asprintf(ret, &size,
			       "mu_address_aget_printable: %s",
			       mu_strerror(rc)))
		return WRDSE_NOSPACE;
	    return WRDSE_USERERR;
	}
    }
    return WRDSE_OK;
}

static void
t_notify(struct mailevt *evt, int ev, wy_triplet_t *trp)
{
    mu_address_t rcpt = NULL;
    const char *errp;
    char *text;
    const char *msg;
    static struct wy_vardef email_def[] = {
	{ "email:admin", expand_email_admin },
	{ "email:owner", expand_email_owner },
	{ NULL }
    };

    switch (evt->rcpt) {
    case notify_read:
	rcpt = NULL;
	break;

    case notify_admin:
	rcpt = mu_address_dup(admin_address);
	break;

    case notify_user:
	rcpt = get_uploader_email(trp, &errp);
	if (!rcpt) {
	    wy_log(LOG_ERR,
		   _("not notifying %s (project %s) about %s: %s"),
		   ntfrcpt_str(evt->rcpt),
		   wy_triplet_project(trp),
		   wy_event_str(ev),
		   gettext(errp));
	    return;
	}
	break;

    case notify_owner:
	rcpt = get_owner_address(trp);
	if (!rcpt) {
	    wy_log(LOG_ERR,
		   _("not notifying %s (project %s) about %s"),
		   ntfrcpt_str(evt->rcpt),
		   wy_triplet_project(trp),
		   wy_event_str(ev));
	    return;
	}
    }

    if (wy_debug_level) {
	if (rcpt) {
	    const char *s;

	    if (mu_address_sget_printable(rcpt, &s) == 0)
		wy_log(LOG_DEBUG,
		       _("notifying %s (project %s) about %s"),
		       s, wy_triplet_project(trp),
		       wy_event_str(ev));
	} else
	    wy_log(LOG_DEBUG,
		   _("notifying message recipients (project %s) "
		     "about %s"),
		   wy_triplet_project(trp),
		   wy_event_str(ev));
    }

    msg = resolve_message_template(evt->msg);
    if (!msg)
	wy_log(LOG_ERR, _("undefined message reference: %s"), evt->msg);
    else if ((text = wy_triplet_expand_param(msg, trp, email_def)) != NULL) {
	mail_send_message(rcpt, text, evt->sign_keys);
	free(text);
    }

    mu_address_destroy(&rcpt);
}


static void
mail_stats(struct mailevt *evt)
{
    const char *tmpl;
    char *text;

    if (!admin_stat_message || !wy_stat_mask_p(mail_admin_mask))
	return;

    if (!admin_address) {
	wy_log(LOG_ERR,
	       _("cannot mail statistics: admin-address not defined"));
	return;
    }

    if (wy_debug_level) {
	const char *s;

	if (mu_address_sget_printable(admin_address, &s) == 0)
	    wy_log(LOG_DEBUG, _("sending stats to %s"), s);
    }

    tmpl = resolve_message_template(admin_stat_message);
    if (!tmpl) {
	wy_log(LOG_ERR, _("undefined message reference: %s"),
	       admin_stat_message);
	return;
    }
    text = wy_expand_stats(tmpl);
    if (text) {
	mail_send_message(admin_address, text, admin_stat_sign_key);
	free(text);
    }
}

void
wy_notify(void *data, int ev, wy_triplet_t *trp)
{
    struct mailevt *evt = data;

    if (trp)
	t_notify(evt, ev, trp);
    else if (ev == wy_ev_stat)
	mail_stats(evt);
}

void
wy_help(void)
{
    static char *docstring = "\n\
Mod_mailutils sends notifications about various Wydawca events\n\
via electronic mail.\n";

    static struct grecs_keyword init_top[] = {
	{ "module-init", "'mailutils",
	  "module initialization",
	  grecs_type_section, GRECS_INAC, NULL, 0, NULL, NULL,
	  mail_kw },
	{ NULL }
    };

    static struct grecs_keyword config_top[] = {
	{ "module-config", NULL,
	  "module configuration",
	  grecs_type_section, GRECS_INAC, NULL, 0, NULL, NULL,
	  notify_event_kw },
	{ NULL }
    };

    puts(docstring);
    printf("Configuration statements:\n\n");
    grecs_print_statement_array(init_top, 1, 0, stdout);

    printf("\n\n# Usage in notify-event statement:\n");
    printf("notify-event {\n  ...");
    grecs_print_statement_array(config_top, 1, 1, stdout);
    printf("}\n");
}
