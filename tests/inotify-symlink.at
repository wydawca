# Check symlink creation in daemon mode                  -*- Autotest -*-
# Copyright (C) 2009-2012, 2017, 2019-2022 Sergey Poznyakoff
#
# Wydawca is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Wydawca is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Wydawca.  If not, see <http://www.gnu.org/licenses/>.

AT_SETUP([Create symlink])
AT_KEYWORDS([inotify symlink inotify-symlink])

AT_WYDAWCA_DAEMON([
AT_SORT_PREREQ
AT_INOTIFY_PREREQ],
[spool test {
    url "ftp://wydawca.test";
    source "$PWD/upload";
    destination "$PWD/dest";
}
],
[mkdir dest/dogana
wy_genfile dogana dest/dogana/dogana-0.file
],
[wy_create_directive upload/symlink.directive.asc dogana \
    symlink: "dogana-0.file dogana.latest" \
    comment: "create a symlink"
while ! test -f $PWD/dest/dogana/dogana.latest && \
      ! test -f $PWD/dest/dogana/dogana.latest.sig
do
  sleep 1
done
],
[find dest -type l | sort
find dest -type l | sort | xargs -n 1 readlink
],
[0],
[dest/dogana/dogana.latest
dest/dogana/dogana.latest.sig
dogana-0.file
dogana-0.file.sig
],
[wydawca: [[NOTICE]] AT_PACKAGE_TARNAME (AT_PACKAGE_NAME AT_PACKAGE_VERSION) started
wydawca: [[NOTICE]] symlink.directive.asc.directive.asc: VERSION: 1.2
wydawca: [[NOTICE]] symlink.directive.asc.directive.asc: COMMENT: create a symlink
wydawca: [[NOTICE]] shutting down on signal "Terminated"
wydawca: [[INFO]] errors: 0
wydawca: [[INFO]] warnings: 0
wydawca: [[INFO]] bad signatures: 0
wydawca: [[INFO]] access violation attempts: 0
wydawca: [[INFO]] complete triplets: 1
wydawca: [[INFO]] incomplete triplets: 0
wydawca: [[INFO]] bad triplets: 0
wydawca: [[INFO]] expired triplets: 0
wydawca: [[INFO]] triplet successes: 1
wydawca: [[INFO]] files uploaded: 0
wydawca: [[INFO]] files archived: 0
wydawca: [[INFO]] symlinks created: 2
wydawca: [[INFO]] symlinks removed: 0
wydawca: [[INFO]] check failures: 0
wydawca: [[NOTICE]] AT_PACKAGE_TARNAME (AT_PACKAGE_NAME AT_PACKAGE_VERSION) finished
])

AT_CLEANUP
