/*
  NAME
     wyclt - simple client for wydawca legacy upload notification protocol

  SYNOPSIS
     wyclt [-w T] SOCKET SPOOL

  DESCRIPTION
     Connects to wydawca server listening on UNIX socket SOCKET and sends
     it upload notification request for SPOOL.

  OPTIONS
     -w T
	Sleep for T seconds between opening the socket and sending the
	request.  This is used to test idle timeout in wydawca server.

  EXIT CODES
     0  success
     1  command line usage error
     2  SIGPIPE received (the word "SIGPIPE" is printed on stderr)

     On any exceptional condition, abort(3) is called.

  LICENSE
     Copyright (C) 2020-2022 Sergey Poznyakoff

     This program is free software; you can redistribute it and/or modify it
     under the terms of the GNU General Public License as published by the
     Free Software Foundation; either version 3 of the License, or (at your
     option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License along
     with this program. If not, see <http://www.gnu.org/licenses/>.
*/
#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <assert.h>
#include <unistd.h>
#include <signal.h>

void
sigpipe(int sig)
{
    fprintf(stderr, "SIGPIPE\n");
    exit(2);
}

int
main(int argc, char **argv)
{
    struct sockaddr_un s_un;
    int fd;
    FILE *fp;
    char buf[128];
    char *p;
    int ttw = 0;
    int c;

    while ((c = getopt(argc, argv, "w:")) != EOF) {
	switch (c) {
	case 'w':
	    ttw = atoi(optarg);
	    assert(ttw >= 0);
	    break;

	default:
	    return 1;
	}
    }
    argc -= optind;
    argv += optind;
    assert(argc == 2);
    assert(strlen(argv[0]) < sizeof(s_un.sun_path));
    strcpy(s_un.sun_path, argv[0]);
    s_un.sun_family = AF_UNIX;
    fd = socket(s_un.sun_family, SOCK_STREAM, 0);
    if (fd == -1) {
	perror("socket");
	abort();
    }

    if (connect(fd, (const struct sockaddr *) &s_un, sizeof(s_un)) == -1) {
	perror("connect");
	abort();
    }

    signal(SIGPIPE, sigpipe);
    if (ttw)
	sleep(ttw);

    fp = fdopen(fd, "w+");
    if (!fp) {
	perror("fdopen");
    }

    fprintf(fp, "%s\r\n", argv[1]);
    p = fgets(buf, sizeof(buf), fp);
    assert(p != NULL);
    if (p[0] != '+') {
	fprintf(stderr, "unexpected: %s\n", buf);
	abort();
    }
    fprintf(fp, "%s\r\n", "user");
    fclose(fp);
    return 0;
}
