/*
  NAME
     wyinit - initialize wydawca testsuite directories

  SYNOPSIS
     wyinit FILE

  DESCRIPTION
     Parses wydawca configuration file FILE.  For each "spool" statement
     found, ensures that the directories listed in its "source" and
     "destination" keywords exist.  Creates missing directories.

  LICENSE
     Copyright (C) 2020-2022 Sergey Poznyakoff

     This program is free software; you can redistribute it and/or modify it
     under the terms of the GNU General Public License as published by the
     Free Software Foundation; either version 3 of the License, or (at your
     option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License along
     with this program. If not, see <http://www.gnu.org/licenses/>.
*/

#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include "grecs.h"

static char *progname;

static void
mkdir_p(char *dir)
{
    char *p;
    struct stat st;

    p = dir;
    if (*p == '/')
	p++;
    while (1) {
	if (*p == 0 || *p == '/') {
	    int c = *p;
	    *p = 0;
	    if (stat(dir, &st)) {
		if (errno == ENOENT) {
		    if (mkdir(dir, 0700)) {
			fprintf(stderr, "%s: cannot create %s: %s\n",
				progname, dir, strerror(errno));
			exit(1);
		    }
		} else {
		    fprintf(stderr, "%s: cannot stat %s: %s\n",
			    progname, dir, strerror(errno));
		    exit(1);
		}
	    } else if (!S_ISDIR(st.st_mode)) {
		fprintf(stderr, "%s: component \"%s\" is not a directory\n",
			progname, dir);
		exit(1);
	    }
	    *p = c;
	    if (c == 0)
		break;
	}
	p++;
    }
}

static void
mkdir_recursive(const char *dir)
{
    char *p = grecs_strdup(dir);
    mkdir_p(p);
    free(p);
}

void
makehier(struct grecs_node *tree, char *path)
{
    struct grecs_node *node;
    grecs_match_buf_t match_buf;

    for (node = grecs_match_first(tree, path, &match_buf);
	 node;
	 node = grecs_match_next(match_buf)) {
	if (node->type != grecs_node_stmt)
	    continue;
	if (node->v.value->type != GRECS_TYPE_STRING)
	    continue;
	mkdir_recursive(node->v.value->v.string);
    }
    grecs_match_buf_free(match_buf);
}

int
main(int argc, char **argv)
{
    struct grecs_node *tree;

    progname = strrchr(argv[0], '/');
    if (!progname)
	progname = argv[0];
    else
	progname++;

    if (argc != 2) {
	fprintf(stderr, "usage: %s FILE\n", progname);
	fprintf(stderr, "creates source and destination directories for wydawca\n");
	fprintf(stderr, "configuration file.\n");
	exit(1);
    }

    grecs_log_to_stderr = 1;
    grecs_parser_options = GRECS_OPTION_QUOTED_STRING_CONCAT;
    tree = grecs_parse(argv[1]);
    if (!tree)
	exit(2);
    makehier(tree, ".spool.source");
    makehier(tree, ".spool.destination");
    return 0;
}
