/*
  NAME
     wyasync - wydawca inotify test tool

  SYNOPSIS
     wyasync [OPTIONS] -- COMMAND

  DESCRIPTION
     Starts two programs: wydawca in foreground mode, and COMMAND.  Waits for
     COMMAND to terminate, shuts down wydawca and exits with the exit code of
     COMMAND.

  OPTIONS
     -c FILE
	 Name of the configuration file.

     -p NAME
	 Full pathname of the "wydawca" binary.

     -t N
	 Time allotted for the COMMAND to terminate.  Default is 60 seconds.

     -w ARG
	 Pass ARG to wydawca verbatim.

  EXIT STATUS
     On error conditions, exits with the following codes:

     64
	 Command line usage error.
     65
	 wydawca terminated prematurely.
     66
	 COMMAND timed out.
     67
	 Operating system error (can't open file, dup fd, etc).
     68
	 Internal software error.

     On success, exits with the exit code of COMMAND.  The latter should not
     use exit codes described above.

  LICENSE
     Copyright (C) 2020-2022 Sergey Poznyakoff

     This program is free software; you can redistribute it and/or modify it
     under the terms of the GNU General Public License as published by the
     Free Software Foundation; either version 3 of the License, or (at your
     option) any later version.

     This program is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License along
     with this program. If not, see <http://www.gnu.org/licenses/>.
 */
#include <config.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

enum {
    EX_OK = 0,
    EX_USAGE = 64,
    EX_WY_TERM = 65,
    EX_TIMEOUT = 66,
    EX_OSERR = 67,
    EX_INTERNAL = 68
};

char *progname;
int timeout = 60;
char *wydawca_prog = "wydawca";
char *wydawca_out = "wydawca.out";
char *wydawca_err = "wydawca.err";

#define MAX_ARGS 64
#define EXTRA_ARGS 3

int w_argc;
char *w_argv[MAX_ARGS+EXTRA_ARGS+1];

pid_t wydawca_pid, command_pid;

pid_t
runcom(char *prog, char **argv, char *out, char *err)
{
    pid_t pid;
    int fd_out, fd_err;

    if (out) {
	fd_out = open(out, O_WRONLY|O_CREAT|O_TRUNC, 0666);
	if (!fd_out) {
	    fprintf(stderr, "%s: can't open %s: %s\n",
		    progname, out, strerror(errno));
	    exit(EX_OSERR);
	}
    } else
	fd_out = -1;
    if (err) {
	fd_err = open(err, O_WRONLY|O_CREAT|O_TRUNC, 0666);
	if (!fd_err) {
	    fprintf(stderr, "%s: can't open %s: %s\n",
		    progname, err, strerror(errno));
	    exit(EX_OSERR);
	}
    } else
	fd_err = -1;

    pid = fork();
    if (pid == -1) {
	fprintf(stderr, "%s: fork: %s\n", progname, strerror(errno));
	exit(EX_OSERR);
    }

    if (pid > 0)
	return pid;

    /* Child */
    switch (fd_out) {
    case -1:
    case 1:
	break;
    default:
	if (dup2(fd_out, 1) == -1) {
	    perror("dup2");
	    exit(EX_OSERR);
	}
	close(fd_out);
    }

    switch (fd_err) {
    case -1:
    case 1:
	break;
    default:
	if (dup2(fd_err, 2) == -1) {
	    perror("dup2");
	    exit(EX_OSERR);
	}
	close(fd_err);
    }

    execvp(prog ? prog : argv[0], argv);
    perror(argv[0]);
    _exit(127);
}

int volatile signum;

void
sighan(int sig)
{
    signum = sig;
}

typedef enum {
    WYASYNC_INITIAL,
    WYASYNC_RUNNING,
    WYASYNC_CHLDEXIT,
    WYASYNC_STOP
} WYASYNC_STATE;

void
usage(void)
{
    printf("usage: %s [OPTIONS] -- COMMAND ARGS...\n", progname);
    printf("wydawca asynchronous test tool\n");
    printf("\nOPTIONS are:\n\n");
    printf("  -c FILE      Name of the configuration file.\n");
    printf("  -p PROG      Full name of the wydawca binary.\n");
    printf("  -t N         Set execution timeout in seconds.\n");
    printf("  -w ARG       Pass ARG to wydawca verbatim.\n");
    printf("\n");
}

int
main(int argc, char **argv)
{
    int c;
    WYASYNC_STATE state;
    int exit_code;
    struct sigaction act;
    sigset_t sigs, oldsigs;

    progname = strrchr(argv[0], '/');
    if (!progname)
	progname = argv[0];
    else
	progname++;

    w_argv[w_argc++] = wydawca_prog = "wydawca";
    while ((c = getopt(argc, argv, "c:hp:t:w:")) != EOF) {
	switch (c) {
	case 'h':
	    usage();
	    exit(EX_OK);

	case 'p':
	    wydawca_prog = optarg;
	    break;

	case 't':
	    timeout = atoi(optarg);
	    if (timeout <= 0) {
		fprintf(stderr, "%s: %s\n", progname, "invalid timeout");
		exit(EX_USAGE);
	    }
	    break;

	case 'c':
	    if (w_argc >= MAX_ARGS - 2) {
		fprintf(stderr, "%s: %s\n", progname,
			"too many arguments for wydawca");
		exit(EX_USAGE);
	    }
	    w_argv[w_argc++] = "-c";
	    w_argv[w_argc++] = optarg;
	    break;

	case 'w':
	    if (w_argc == MAX_ARGS) {
		fprintf(stderr, "%s: %s\n", progname,
			"too many arguments for wydawca");
		exit(EX_USAGE);
	    }
	    w_argv[w_argc++] = optarg;
	    break;

	default:
	    exit(EX_USAGE);
	}
    }

    argc -= optind;
    argv += optind;
    if (argc == 0) {
	fprintf(stderr, "%s: %s\n", progname, "command not given");
	exit(EX_USAGE);
    }

    /* Finalize wydawca arguments */
    w_argv[w_argc++] = "--daemon";
    w_argv[w_argc++] = "--foreground";
    w_argv[w_argc++] = "--stderr";
    w_argv[w_argc] = NULL;

    /* Install signals */
    act.sa_flags = 0;
    sigemptyset(&act.sa_mask);
    act.sa_handler = sighan;

    sigaction(SIGCHLD, &act, NULL);
    sigaction(SIGALRM, &act, NULL);
    sigaction(SIGUSR1, &act, NULL);

    sigemptyset(&sigs);
    sigaddset(&sigs, SIGCHLD);
    sigaddset(&sigs, SIGALRM);
    sigaddset(&sigs, SIGUSR1);
    sigprocmask(SIG_BLOCK, &sigs, &oldsigs);

    /* Start wydawca */
    if (setenv("WYDAWCA_NOTIFY_PARENT", "1", 1)) {
	fprintf(stderr, "%s: failed to modify environment\n", progname);
	exit(EX_OSERR);
    }
    wydawca_pid = runcom(wydawca_prog, w_argv, wydawca_out, wydawca_err);

    /* Set timeout */
    alarm(timeout);

    state = WYASYNC_INITIAL;
    while (state != WYASYNC_STOP) {
	pid_t pid;
	int status;

	sigsuspend(&oldsigs);
	switch (signum) {
	case SIGUSR1:
	    /* Start command */
	    command_pid = runcom(NULL, argv, NULL, NULL);
	    state = WYASYNC_RUNNING;
	    break;

	case SIGCHLD:
	    while ((pid = waitpid((pid_t)-1, &status, WNOHANG)) > 0) {
		if (pid == wydawca_pid) {
		    wydawca_pid = -1;
		    switch (state) {
		    case WYASYNC_INITIAL:
			exit_code = EX_WY_TERM;
			state = WYASYNC_STOP;
			break;

		    case WYASYNC_RUNNING:
			kill(command_pid, SIGTERM);
			exit_code = EX_WY_TERM;
			state = WYASYNC_CHLDEXIT;
			break;

		    case WYASYNC_CHLDEXIT:
			state = WYASYNC_STOP;
			break;

		    default:
			abort();
		    }
		} else if (pid == command_pid) {
		    command_pid = -1;
		    switch (state) {
		    case WYASYNC_RUNNING:
			kill(wydawca_pid, SIGTERM);
			exit_code = WEXITSTATUS(status);
			state = WYASYNC_CHLDEXIT;
			break;

		    case WYASYNC_CHLDEXIT:
			state = WYASYNC_STOP;
			break;

		    default:
			abort();
		    }
		}
	    }
	    break;

	case SIGALRM:
	    state = WYASYNC_STOP;
	    exit_code = EX_TIMEOUT;
	}
    }

    exit(exit_code);
}
