# This file is part of wydawca testsuite.      -*- Autotest -*-
# Copyright (C) 2009-2012, 2017, 2019-2022 Sergey Poznyakoff
#
# Wydawca is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Wydawca is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Wydawca.  If not, see <http://www.gnu.org/licenses/>.
AT_SETUP([Failed upload])
AT_KEYWORDS([upload upload-single upload-failure dry-run])
AT_CHECK_UPLOAD([
spool test {
    url "ftp://wydawca.test";
    source "$PWD/upload";
    destination "$PWD/dest";
}
],
[wy_create_upload -u forese@example.com conversion upload/conversion-1.1.tar
set -e
wydawca --no-preproc -c wydawca.cf 2>err

# FIXME: The incomplete triplets count can be set to 1 if the directive
# file is seen first.  In this case it is parsed early, found to be signed
# by wrong person (which also raises the bad signatures and access violation
# attempts counts) and the corresponding triplet is removed.  Then, the
# archive and signature files are read and combined into a single inclomplete
# triplet.  Incomplete because the directive part has already been dismissed.
# This triplet is detected at the end of the run.
#
# If, on the other hand, the archive and signature were read first and the
# directive file last, then the completed triplet would be thrown out and
# incomplete triplet count would remain 0.
#
# Ways out:
#  1. Detain the triplet in table until it is completed and only then
#     verify the signature.  This will always produce the second case.
#     However, it implies parsing the signature file early.  Actually,
#     that's the way wydawca operates now. However, although it seemed
#     a nice idea at the time, now I incline to the second solution.
#  2. Verify the triplet as soon as the directive file is read.  Parse
#     the directive only if the signature passes verification.  Otherwise,
#     drop it.  This is pretty similar to how wydawca operates now, except
#     that the directive file would be parsed only if verified.  That seems
#     to be an advantage, since that's what the signature is for, in the
#     first place.  Secondly, that would allow to use right GPGME function
#     for extracting the file content, instead of feedling with it manually.
#     The drawback is that processing of badly signed directives would
#     sometimes produce false incomplete triplets.  The only fix is to defer
#     triplet processing to the end of directory scan, which implies a
#     memory usage overhead.
sed -e 's/incomplete triplets: 1/incomplete triplets: 0/' \
    -e 's/No public key:.*$/No public key/' \
    -e '/removing/d' err >&2

echo "dest dir"
find dest
echo "upload dir"
find upload -name conversion-1.1.tar.directive.asc
],
[0],
[dest dir
dest
upload dir
],
[wydawca: [[NOTICE]] AT_PACKAGE_TARNAME (AT_PACKAGE_NAME AT_PACKAGE_VERSION) started
wydawca: [[NOTICE]] conversion-1.1.tar.directive.asc: VERSION: 1.2
wydawca: [[ERR]] No public key
wydawca: [[ERR]] invalid signature for conversion-1.1.tar
wydawca: [[INFO]] errors: 0
wydawca: [[INFO]] warnings: 0
wydawca: [[INFO]] bad signatures: 1
wydawca: [[INFO]] access violation attempts: 1
wydawca: [[INFO]] complete triplets: 0
wydawca: [[INFO]] incomplete triplets: 0
wydawca: [[INFO]] bad triplets: 0
wydawca: [[INFO]] expired triplets: 0
wydawca: [[INFO]] triplet successes: 0
wydawca: [[INFO]] files uploaded: 0
wydawca: [[INFO]] files archived: 0
wydawca: [[INFO]] symlinks created: 0
wydawca: [[INFO]] symlinks removed: 0
wydawca: [[INFO]] check failures: 0
wydawca: [[NOTICE]] AT_PACKAGE_TARNAME (AT_PACKAGE_NAME AT_PACKAGE_VERSION) finished
])
AT_CLEANUP
