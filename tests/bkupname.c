/* wordsplit - a word splitter
   Copyright (C) 2009-2011, 2017, 2019-2022 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <config.h>
#include "wydawca.h"

char *progname;

void
usage(int code)
{
    FILE *fp = code ? stderr : stdout;

    fprintf(fp,
	    "usage: %s [-no] [-simple] [-existing] [-numbered] [-suffix=S] FILE\n",
	    progname);
    exit(code);
}

int
main(int argc, char **argv)
{
    enum backup_type type = numbered_backups;
    char *file = NULL;
    char *backup;

    progname = argv[0];
    while (--argc) {
	char *arg = *++argv;
	if (strcmp(arg, "-no") == 0)
	    type = no_backups;
	else if (strcmp(arg, "-simple") == 0)
	    type = simple_backups;
	else if (strcmp(arg, "-existing") == 0)
	    type = numbered_existing_backups;
	else if (strcmp(arg, "-numbered") == 0)
	    type = numbered_backups;
	else if (strncmp(arg, "-suffix=", 8) == 0)
	    simple_backup_suffix = arg + 8;
	else if (arg[0] == '-')
	    usage(2);
	else {
	    if (argc != 1)
		usage(2);
	    file = arg;
	}
    }

    if (!file)
	usage(2);
    backup = find_backup_file_name(AT_FDCWD, file, type);
    if (!backup) {
	if (errno) {
	    perror(file);
	    exit(1);
	}
	printf("no backup\n");
    } else
	printf("%s\n", backup);
    free(backup);
    exit(0);
}
