/* wydawca - automatic release submission daemon
   Copyright (C) 2007-2022 Sergey Poznyakoff

   Wydawca is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   Wydawca is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with wydawca. If not, see <http://www.gnu.org/licenses/>. */

#include <mailutils/mailutils.h>

char const *headers[] = {
    MU_HEADER_FROM,
    MU_HEADER_TO,
    MU_HEADER_SUBJECT,
    NULL
};

char *output_name;
char *sender_email;

static struct mu_option nullmail_options[] = {
    { "output", 'o', "FILE", MU_OPTION_DEFAULT,
      "dump mail to file",
      mu_c_string, &output_name },
    { "from", 'F', "EMAIL", MU_OPTION_DEFAULT,
      "sender email address",
      mu_c_string, &sender_email },
    MU_OPTION_END
}, *options[] = { nullmail_options, NULL };

struct mu_cli_setup cli = {
    options,
    NULL,
    "null mailer",
    NULL
};

char *capa[] = {
    "debug",
    NULL
};

int
main(int argc, char **argv)
{
    int i, rc;
    mu_message_t msg;
    mu_stream_t input, output, str;
    mu_header_t hdr;
    mu_body_t body;

    mu_cli(argc, argv, &cli, capa, NULL, &argc, &argv);
    /* FIXME: non-option arguments are silently ignored */
    if (output_name)
	MU_ASSERT(mu_file_stream_create (&output, output_name,
					 MU_STREAM_CREAT|MU_STREAM_APPEND));
    else {
	output = mu_strout;
	mu_stream_ref(output);
    }

  if (sender_email)
    mu_stream_printf(output, "From %s\n", sender_email);

  MU_ASSERT(mu_temp_file_stream_create(&input, NULL, 0));
  MU_ASSERT(mu_stream_copy(input, mu_strin, 0, NULL));
  MU_ASSERT(mu_stream_to_message(input, &msg));
  MU_ASSERT(mu_message_get_header(msg, &hdr));
  for (i = 0; headers[i]; i++) {
      char *val;
      rc = mu_header_aget_value_unfold(hdr, headers[i], &val);
      if (rc == 0) {
	  mu_stream_printf(output, "%s: %s\n", headers[i], val);
	  free(val);
      }
      else if (rc != MU_ERR_NOENT)
	  mu_diag_funcall(MU_DIAG_ERROR, "mu_header_get_value_unfold",
			  headers[i], rc);
    }
  mu_stream_printf(output, "\n");
  MU_ASSERT(mu_message_get_body(msg, &body));
  MU_ASSERT(mu_body_get_streamref(body, &str));
  MU_ASSERT(mu_stream_copy(output, str, 0, NULL));

  mu_stream_close(output);

  return 0;
}
