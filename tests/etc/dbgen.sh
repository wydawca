#!/bin/sh
# Usage: dbgen.sh [--gpg GPG] [--homedir DIR] INFILE

GNUPGHOME=gnupg
export GNUPGHOME

while [ $# -ne 0 ]
do
    case $1 in
	--gpg)
	    GPG=$2
	    shift 2
	    ;;
	--homedir)
	    GNUPGHOME=$2
	    shift 2
	    ;;
	--output)
	    output=$2
	    shift 2
	    ;;
        --output=*)
	    output=${1##--output=}
            shift	
	    ;;
	-o)
   	    output=$2
	    shift 2
	    ;;
	-o*)
	    output=${1##-o}
	    shift
	    ;;
        --)
	    shift
	    ;;
	--*)
	    echo >&2 "$0: unrecognized option $1"
	    exit 1
	    ;;
	*)  break       
    esac
done    

if [ $# -eq 1 ]; then
    projects_db=$1
else    
    echo >&2 "$0: exactly one argument must be given"
    exit 1
fi

if [ -n "$output" ]; then
    exec >$output
fi    

cat <<\EOF
dictionary project-owner {
  type builtin;
  query "${project}";
  params ("/exact",
EOF

oIFS=$IFS
IFS=":"
while read project realname email comment 
do
    IFS=$oIFS
    echo "          \"$project\", \"$email\", \"$realname\","
    IFS=":"
done < $projects_db | sed -e '$s/,$/);/'
IFS=$oIFS

echo "}"

cat <<\EOF
dictionary project-uploader {
  type builtin;
  query "${project}";
  params ("/exact",
EOF

IFS=":"
while read project realname email comment
do
    IFS=$oIFS
    user=${email%%@*}
    echo "          \"$project\", \"$user\", \"$comment\", \"$email\","
    ${GPG:-gpg2} --quiet --no-permission-warning --batch \
		 --export --armor "$realname" |
	sed -r -e 's/.*/"&\\n"/' -e '$s/$/,/'
    IFS=":"
done < $projects_db | sed -e '$s/,$/);/'
echo "}"
