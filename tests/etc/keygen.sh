#!/bin/sh
# Usage: keygen.sh [--gpg GPG] --homedir DIR [INFILE]

unset GNUPGHOME
while [ $# -ne 0 ]
do
    case $1 in
	--gpg)
	    GPG=$2
	    shift 2
	    ;;
	--homedir)
	    GNUPGHOME=$2
	    shift 2
	    ;;
        --)
	    shift
	    ;;
	--*)
	    echo >&2 "$0: unrecognized option $1"
	    exit 1
	    ;;
	*)  break       
    esac
done    

if [ $# -eq 1 ]; then
    exec <$1
fi

: ${GNUPGHOME:?the --homedir option must be given}
if [ -d $GNUPGHOME ]; then
    rm -rf $GNUPGHOME/*
else
    mkdir $GNUPGHOME
fi    
export GNUPGHOME

oIFS=$IFS
IFS=':'
while read project realname email comment
do
    IFS=$oIFS
    cat <<EOF
%echo Generating OpenPGP key for user $realname
Key-Type: RSA
Key-Length: 2048
Subkey-Type: ELG-E
Subkey-Length: 2048
Name-Real: $realname
Name-Comment: $comment
Name-Email: $email
Expire-Date: 0
%no-protection
%transient-key
%commit
EOF
    IFS=':'
done | ${GPG:-gpg2} --quiet --no-permission-warning --batch --gen-key 
