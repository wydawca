# This file is part of Wydawca -*- autoconf -*-
# Copyright (C) 2007-2023 Sergey Poznyakoff
#
# Wydawca is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3, or (at your option)
# any later version.
#
# Wydawca is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with wydawca.  If not, see <http://www.gnu.org/licenses/>.

AC_PREREQ([2.71])
AC_INIT([wydawca], 4.0.90, [bug-wydawca@gnu.org.ua], [wydawca],
        [http://www.gnu.org.ua/software/wydawca])
AC_CONFIG_SRCDIR([src/wydawca.c])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_MACRO_DIRS([m4])
AM_INIT_AUTOMAKE([1.16.5 gnits tar-ustar dist-bzip2 dist-xz std-options silent-rules subdir-objects])

# Enable silent rules by default:
AM_SILENT_RULES([yes])

# Checks for programs.
AC_PROG_CC
dnl AC_PROG_RANLIB
AC_USE_SYSTEM_EXTENSIONS

LT_INIT([dlopen])
AC_SUBST(WYDAWCA_MODDIR,'$(libdir)/$(PACKAGE)') 

# Checks for libraries.
AC_CHECK_LIB(rt, clock_gettime)

# Checks for typedefs, structures, and compiler characteristics.
AC_TYPE_UID_T
AC_TYPE_PID_T
AC_TYPE_SIZE_T
AC_HEADER_STDBOOL
AC_SYS_LARGEFILE

AC_CHECK_MEMBERS([struct tm.tm_gmtoff],,,
                 [#include <sys/types.h>
#include <time.h>
])

# Checks for library functions.
AC_FUNC_VPRINTF
AC_CHECK_FUNCS([setegid setregid setresgid setresuid seteuid \
 setreuid vsyslog])

# Check for POSIX threads support
AX_PTHREAD([],
 [AC_MSG_ERROR([POSIX threads support is required, but not available])])

AX_THREAD_NAMES
AX_CLOSEFROM

# **********************	
# Mailutils
# **********************	

AM_GNU_MAILUTILS(3.3, [mailer], [status_mailutils=yes], [status_mailutils=no])
AM_CONDITIONAL([COND_MAILUTILS],[test $status_mailutils = yes])
AC_SUBST(WY_MAILUTILS,[$status_mailutils])

# **********************	
# MySQL	
# **********************	

AC_SUBST(SQLLIB)		
AH_TEMPLATE(HAVE_MYSQL,[Define if you have libmysql])

WY_CHECK_LIB(mysqlclient, 
             mysql_real_connect, 
	     [-lm],
             [ AC_DEFINE(HAVE_MYSQL)
	       SQLLIB="$wd_cv_lib_mysqlclient" ],
	     [ AC_MSG_FAILURE([The required library libmysqlclient is not found or is unusable]) ],
             [/usr/lib/mysql /usr/local/lib/mysql])

# **********************	
# GPGME
# **********************	

AC_SUBST(GPGMELIB)
AH_TEMPLATE(HAVE_GPGME,[Define if you have gpgme])

AC_CHECK_HEADERS(gpgme.h)
WY_CHECK_LIB([gpgme],[main],[],
             [GPGMELIB="$wd_cv_lib_gpgme"
              AC_DEFINE(HAVE_GPGME) ],
	     [ AC_MSG_FAILURE([The requested library libgpgme is not found or is unusable])],
	     [/usr/pkg/lib /opt/lib /sw/lib])

saved_LDADD=$LDADD
LDADD="$LDADD $GPGMELIB"
AC_CHECK_FUNC(gpgme_set_offline)
AH_BOTTOM(
[#ifndef HAVE_GPGME_SET_OFFLINE
# define gpgme_set_offline(c,f)
#endif
])
LDADD=$saved_LDADD	     

# **********************	
# TCP wrappers
# **********************	
AC_ARG_WITH(tcp-wrappers,
	AS_HELP_STRING([--with-tcp-wrappers],[compile with TCP wrappers support (default)]),
	[status_tcpwrap=${withval}],
	[status_tcpwrap=yes])

if test "$status_tcpwrap" = yes; then
  AC_CHECK_LIB(nsl, main)
  AC_CHECK_LIB(wrap, main,, [status_tcpwrap=no])
  if test "$status_tcpwrap" = yes; then
    AC_CHECK_HEADERS(tcpd.h,,[status_tcpwrap=no])
  fi
fi
if test "$status_tcpwrap" = yes; then
  AC_DEFINE_UNQUOTED([WITH_LIBWRAP],1,[Defined if compiling with libwrap])
fi  

# **********************	
# Inotify
# **********************	
# Checks for header files.
AC_CHECK_HEADERS([sys/inotify.h])
AC_CHECK_FUNCS([inotify_init])
AC_ARG_WITH(inotify,
	AS_HELP_STRING([--with-inotify],[compile with inotify(7) support (Linux-specific)]),
	[status_inotify=${withval}],
	[status_inotify=probe])
if test $status_inotify != no; then
  if test "$ac_cv_header_sys_inotify_h" = yes &&
	     test "$ac_cv_func_inotify_init" = yes; then
    status_inotify=yes
  elif test $status_inotify = probe; then
    status_inotify=no
  else
    AC_MSG_FAILURE([Requested inotify(7) support is not available])
  fi
fi
if test $status_inotify = yes; then
  AC_DEFINE([WITH_INOTIFY],1,[Set to 1 if inotify(7) is to be used])
fi  
AM_CONDITIONAL([COND_INOTIFY],[test $status_inotify = yes])
AC_SUBST(WY_INOTIFY,[$status_inotify])
	
# Grecs subsystem

GRECS_SETUP([grecs],[tree-api git2chg getopt tests shared install-headers
                     std-pp-setup])
AC_CONFIG_LINKS(include/wydawca/wordsplit.h:grecs/wordsplit/wordsplit.h)

AH_BOTTOM([
#if __GNUC__ < 2 || (__GNUC__ == 2 && __GNUC_MINOR__ < 7)
#  define __attribute__(x)
#endif

])
	
# Initialize the test suite.
AC_CONFIG_TESTDIR(tests)
AC_CONFIG_FILES([tests/Makefile tests/atlocal tests/etc/Makefile])
AM_MISSING_PROG([AUTOM4TE], [autom4te])

# Initialize documentation helpers.
IMPRIMATUR_INIT(, [frenchspacing])

# *********************************************************************
# Preprocessor
# *********************************************************************
AC_ARG_VAR([DEFAULT_INCLUDE_PATH],
           [Include path for configuration preprocessor])
if test -z "$DEFAULT_INCLUDE_PATH"; then
  DEFAULT_INCLUDE_PATH='$(pkgdatadir)/include:$(pkgdatadir)/$(VERSION)/include'
fi

# *********************************************************************
# GPG v2 (for the testsuite)
# The testsuite needs to create GPG keys, which would be prohibitively
# slow without the %transient-key statement, which appeared in GPG 2.1
# *********************************************************************
AC_ARG_VAR([GPG],[Name of the gpg version 2 binary])
AC_MSG_CHECKING([for the GPG v2 binary])
uGPG=$GPG
unset GPG
gpgver() {
    GPG_VERSION_STRING=$($1 --version|head -n 1|sed -e 's/.* //')
    oIFS=$IFS
    IFS="."
    set -- $GPG_VERSION_STRING
    GPG_VERSION_MAJOR=$1
    GPG_VERSION_MINOR=$2
    IFS=$oIFS
}
for prog in $uGPG gpg2 gpg
do
   gpgver $prog
   if test -n "$GPG_VERSION_MAJOR" -a -n "$GPG_VERSION_MINOR"; then
       if test $GPG_VERSION_MAJOR -eq 2; then
           if test $GPG_VERSION_MINOR -ge 1; then
	       GPG=$prog
	       break
	   fi
       elif test $GPG_VERSION_MAJOR -gt 2; then
           GPG=$prog
           break
       fi
   fi
done
AC_MSG_RESULT([${GPG:-none}${GPG:+, $GPG_VERSION_STRING}])
AM_CONDITIONAL([COND_GPG2],[test -n "$GPG"])

AC_CONFIG_FILES([Makefile
                 doc/Makefile
		 include/Makefile
		 include/wydawca/Makefile
		 src/Makefile
		 modules/Makefile
		 modules/logstat/Makefile
		 modules/mailutils/Makefile
                 etc/Makefile])
		 
AC_OUTPUT
